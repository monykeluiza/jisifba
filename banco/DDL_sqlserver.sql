

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table perfil
-- -----------------------------------------------------
CREATE TABLE  perfil (
  id INT NOT NULL IDENTITY(1,1),
  nome VARCHAR(45) NOT NULL,
  PRIMARY KEY (id))
;


-- -----------------------------------------------------
-- Table user
-- -----------------------------------------------------
CREATE TABLE  usuario (
  username VARCHAR(16) NOT NULL,
  email VARCHAR(255) NULL,
  password VARCHAR(32) NULL,
  create_time DATETIME NULL,
  active TINYINT NOT NULL DEFAULT 1,
  admin TINYINT NULL DEFAULT 0,
  id INT NOT NULL IDENTITY(1,1),
  ldap TINYINT NOT NULL DEFAULT 1,
  data_ultimo_acesso DATETIME NULL,
  perfil_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_perfil1
    FOREIGN KEY (perfil_id)
    REFERENCES perfil (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE INDEX fk_user_perfil1_idx ON usuario (perfil_id ASC);


-- -----------------------------------------------------
-- Table campus
-- -----------------------------------------------------
CREATE TABLE  campus (
  id INT NOT NULL IDENTITY(1,1),
  nome VARCHAR(200) NOT NULL,
  sigla VARCHAR(45) NOT NULL,
  vagas INT NOT NULL,
  PRIMARY KEY (id))
;


-- -----------------------------------------------------
-- Table setor
-- -----------------------------------------------------
CREATE TABLE  setor (
  id INT NOT NULL IDENTITY(1,1),
  nome VARCHAR(200) NOT NULL,
  sigla VARCHAR(45) NOT NULL,
  campus_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_setor_campus1
    FOREIGN KEY (campus_id)
    REFERENCES campus (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_setor_campus1_idx ON setor (campus_id ASC);


-- -----------------------------------------------------
-- Table servidor
-- -----------------------------------------------------

CREATE TABLE  servidor (
  id INT NOT NULL IDENTITY(1,1),
  nome VARCHAR(250) NOT NULL,
  data_nascimento DATE NOT NULL,
  email_institucional VARCHAR(255) NULL,
  siape VARCHAR(10) NULL,
  email_pessoal VARCHAR(255) NULL,
  sexo CHAR NOT NULL,
  cpf VARCHAR(15) NOT NULL,
  usuario_id INT NULL,
  setor_id INT NULL,
  is_diretor_campus TINYINT NOT NULL DEFAULT 0,
  is_chefe_delegacao TINYINT NOT NULL DEFAULT 0,
  hospedagem TINYINT NULL,
  qtd_acompanhantes_adultos INT NULL,
  qtd_acompanhantes_criancas INT NULL,
  plano_de_saude VARCHAR(200) NULL,
  alergias VARCHAR(2000) NULL,
  doencas VARCHAR(2000) NULL,
  nome_contato_emergencia VARCHAR(200) NULL,
  telefone_contato_emergencia VARCHAR(45) NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_servidor_user
    FOREIGN KEY (usuario_id)
    REFERENCES usuario (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_servidor_setor1
    FOREIGN KEY (setor_id)
    REFERENCES setor (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_servidor_user_idx ON servidor (usuario_id ASC);

CREATE INDEX fk_servidor_setor1_idx ON servidor (setor_id ASC);


-- -----------------------------------------------------
-- Table tipo
-- -----------------------------------------------------
CREATE TABLE  tipo (
  id INT NOT NULL IDENTITY(1,1),
  nome VARCHAR(45) NOT NULL,
  PRIMARY KEY (id))
;


-- -----------------------------------------------------
-- Table modalidade
-- -----------------------------------------------------
CREATE TABLE  modalidade (
  id INT NOT NULL IDENTITY(1,1),
  nome VARCHAR(100) NOT NULL,
  ativa TINYINT NOT NULL DEFAULT 1,
  tipo_id INT NOT NULL,
  qtd_titular INT NOT NULL,
  qtd_reserva INT NOT NULL,
  sexo VARCHAR(45) NOT NULL,
  vaga_por_campus INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_modalidade_tipo1
    FOREIGN KEY (tipo_id)
    REFERENCES tipo (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_modalidade_tipo1_idx ON modalidade (tipo_id ASC);


-- -----------------------------------------------------
-- Table equipe
-- -----------------------------------------------------
CREATE TABLE  equipe (
  id INT NOT NULL IDENTITY(1,1),
  nome VARCHAR(100) NOT NULL,
  modalidade VARCHAR(45) NOT NULL,
  PRIMARY KEY (id))
;

-- -----------------------------------------------------
-- Table status
-- -----------------------------------------------------
CREATE TABLE  status (
  id INT NOT NULL IDENTITY(1,1),
  nome VARCHAR(100) NOT NULL,
  PRIMARY KEY (id))
;

-- -----------------------------------------------------
-- Table inscricao
-- -----------------------------------------------------
CREATE TABLE  inscricao (
  servidor_id INT NOT NULL,
  modalidade_id INT NOT NULL,
  data DATETIME NOT NULL,
  atestado varbinary(max) NULL,
  equipe_id INT NULL,
  time_avulso TINYINT NULL,
  titular TINYINT NULL,
  status_id INT NOT NULL,
  PRIMARY KEY (servidor_id, modalidade_id),
  CONSTRAINT fk_servidor_has_modalidade_servidor1
    FOREIGN KEY (servidor_id)
    REFERENCES servidor (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_servidor_has_modalidade_modalidade1
    FOREIGN KEY (modalidade_id)
    REFERENCES modalidade (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_inscricao_equipe1
    FOREIGN KEY (equipe_id)
    REFERENCES equipe (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT fk_inscricao_status1
    FOREIGN KEY (status_id)
    REFERENCES status (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_servidor_has_modalidade_modalidade1_idx ON inscricao (modalidade_id ASC);

CREATE INDEX fk_servidor_has_modalidade_servidor1_idx ON inscricao (servidor_id ASC);

CREATE INDEX fk_inscricao_equipe1_idx ON inscricao (equipe_id ASC);

CREATE INDEX fk_inscricao_status1_idx ON inscricao(status_id ASC);




-- -----------------------------------------------------
-- Table calendario
-- -----------------------------------------------------
CREATE TABLE  calendario (
  id INT NOT NULL IDENTITY(1,1),
  data DATE NOT NULL,
  hora_inicio TIME NOT NULL,
  hora_fim TIME NULL,
  local VARCHAR(100) NOT NULL,
  descricao VARCHAR(600) NULL,
  modalidade_id INT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_calendario_modalidade1
    FOREIGN KEY (modalidade_id)
    REFERENCES modalidade (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_calendario_modalidade1_idx ON calendario (modalidade_id ASC);


-- -----------------------------------------------------
-- Table tipo_doc
-- -----------------------------------------------------
CREATE TABLE  tipo_doc (
  id INT NOT NULL IDENTITY(1,1),
  nome VARCHAR(45) NOT NULL,
  PRIMARY KEY (id))
;


-- -----------------------------------------------------
-- Table docs
-- -----------------------------------------------------
CREATE TABLE  docs (
  id INT NOT NULL IDENTITY(1,1),
  documento varbinary(max) NOT NULL,
  data DATETIME NOT NULL,
  nome_arquivo VARCHAR(100) NOT NULL,
  servidor_id INT NOT NULL,
  tipo_doc_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_docs_servidor1
    FOREIGN KEY (servidor_id)
    REFERENCES servidor (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_docs_tipo_doc1
    FOREIGN KEY (tipo_doc_id)
    REFERENCES tipo_doc (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_docs_servidor1_idx ON docs (servidor_id ASC);

CREATE INDEX fk_docs_tipo_doc1_idx ON docs (tipo_doc_id ASC);


-- -----------------------------------------------------
-- Table log
-- -----------------------------------------------------
CREATE TABLE log (
  id int NOT NULL IDENTITY(1,1),
  data datetime NOT NULL,
  id_usuario int NOT NULL,
  login varchar(100) NOT NULL,
  nome varchar(200) NOT NULL,
  id_tabela int NOT NULL,
  acao varchar(45) NOT NULL,
  nome_tabela varchar(100) NOT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE  campus_modalidade (
  campus_id INT NOT NULL,
  modalidade_id INT NOT NULL,
  vagas INT NOT NULL,
  PRIMARY KEY (campus_id, modalidade_id),
  CONSTRAINT fk_campus_has_modalidade_campus1
    FOREIGN KEY (campus_id)
    REFERENCES campus (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_campus_has_modalidade_modalidade1
    FOREIGN KEY (modalidade_id)
    REFERENCES modalidade (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

alter table usuario alter column username varchar(50) NOT NULL;