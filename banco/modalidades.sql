insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO 100M RASOS', 1, 1, 2,0,'M',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO 100M RASOS', 1, 1, 2,0,'F',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO SALTO EM DIST�NCIA', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO SALTO EM DIST�NCIA', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO ARREMESSO DE PESO', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO ARREMESSO DE PESO', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO LAN�AMENTO DE DARDO', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO LAN�AMENTO DE DARDO', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 50M LIVRE', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 50M LIVRE', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 50M PEITO', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 50M PEITO', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 50M COSTA', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 50M COSTA', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 50M BORBOLETA', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 50M BORBOLETA', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 100M LIVRE', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 100M LIVRE', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 100M PEITO', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 100M PEITO', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 100M COSTA', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 100M COSTA', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 100M MEDLEY', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 100M MEDLEY', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 200M LIVRE', 1, 1, 2,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O 200M LIVRE', 1, 1, 2,0,'M',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O REVEZAMENTO 4X50M LIVRE', 1, 2, 4,0,'F',4);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O REVEZAMENTO 4X50M LIVRE', 1, 2, 4,0,'M',4);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O REVEZAMENTO 4X50M MEDLEY', 1, 2, 4,0,'F',4);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('NATA��O REVEZAMENTO 4X50M MEDLEY', 1, 2, 4,0,'M',4);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('T�NIS DE MESA', 1, 1, 2,0,'M',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('T�NIS DE MESA', 1, 1, 2,0,'F',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('T�NIS DE MESA', 1, 2, 2,0,'M',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('T�NIS DE MESA', 1, 2, 2,0,'F',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('CORRIDA DE 10KM', 1, 1, 1,0,'M',1);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('CORRIDA DE 10KM', 1, 1, 1,0,'F',1);


insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('DAMA', 1, 1, 2,0,'M',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('DAMA', 1, 1, 2,0,'F',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('DOMIN�', 1, 2, 2,0,'M',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('DOMIN�', 1, 2, 2,0,'F',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('FIFA 2017', 1, 1, 2,0,'MF',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('JUST DANCE', 1, 1, 2,0,'MF',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('XADREZ', 1, 1, 1,0,'MF',2);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO 1.500M RASOS', 1, 1, 1,0,'F',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO 3000M RASOS', 1, 1, 1,0,'M',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO SALTO EM ALTURA', 1, 1, 1,0,'M',2);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO REVEZAMENTO 4X100', 1, 2, 4,0,'M',4);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('ATLETISMO REVEZAMENTO 4X100', 1, 2, 4,0,'F',4);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('BALEADO', 1, 2, 10,2,'MF',12);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('BASQUETEBOL', 1, 2, 5,5,'MF',10);

insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('FUTSAL', 1, 2, 5,5,'M',10);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('FUTEBOL SOCIETY', 1, 2, 8,7,'M',15);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('VOLEIBOL', 1, 2, 6,6,'MF',12);
insert into modalidade (nome, ativa, tipo_id, qtd_titular, qtd_reserva, sexo, vaga_por_campus) values ('V�LEI DE AREIA 4X4', 1, 2, 4,0,'MF',4);

UPDATE modalidade SET tipo_id = 1 where nome like 'NATA��O%REVEZAMENTO%';
UPDATE modalidade SET tipo_id = 1 where nome like 'ATLETISMO%REVEZAMENTO%';
UPDATE MODALIDADE SET NOME = 'CORRIDA DE 5KM' WHERE nome like 'CORRIDA%';










