insert into perfil (nome) values ('servidor'),('chefe de delega��o'),('Diretor'),('Comiss�o Local'), ('Comiss�o central');

insert into tipo_doc (nome) values ('Documento'),('Atestado'),('Foto'),('Termo de Responsabilidade');

insert into tipo (nome) values ('Individual'),('Coletivo');

insert into usuario (username, create_time, active, admin, perfil_id) values
('moniqueluiza', null, 1, 1, 5); 

  insert into campus (nome, sigla, vagas) values ('Salvador','SSA', 67);  
  insert into campus (nome, sigla, vagas) values ('Jacobina','JAC',9);  
  insert into campus (nome, sigla, vagas) values ('Euclides de Cunha','EC', 5);  
  insert into campus (nome, sigla, vagas) values ('Brumado','BRU', 2);  
  insert into campus (nome, sigla, vagas) values ('Irec�','IRE', 10);  
  insert into campus (nome, sigla, vagas) values ('Ubaitaba','UBA', 1);  
  insert into campus (nome, sigla, vagas) values ('Eun�polis','EUN', 17);  
  insert into campus (nome, sigla, vagas) values ('Ilh�us','ILH', 10);  
  insert into campus (nome, sigla, vagas) values ('Santo Ant�nio de Jesus','SAJ', 4);  
  insert into campus (nome, sigla, vagas) values ('Lauro de Freitas','LF', 1);  
  insert into campus (nome, sigla, vagas) values ('Santo Amaro','SAM', 14);  
  insert into campus (nome, sigla, vagas) values ('Barreiras','BAR', 18);  
  insert into campus (nome, sigla, vagas) values ('Seabra','SEA', 7);  
  insert into campus (nome, sigla, vagas) values ('Vit�ria da Conquista','VC',22);  
  insert into campus (nome, sigla, vagas) values ('Porto Seguro','PS',12);  
  insert into campus (nome, sigla, vagas) values ('Sim�es Filho','SF',13);  
  insert into campus (nome, sigla, vagas) values ('Reitoria','REI',27);  
  insert into campus (nome, sigla, vagas) values ('Cama�ari','CAM',12);  
  insert into campus (nome, sigla, vagas) values ('Juazeiro','JUA',6);  
  insert into campus (nome, sigla, vagas) values ('Paulo Afonso','PA',12);  
  insert into campus (nome, sigla, vagas) values ('Valen�a','VAL',13);  
  insert into campus (nome, sigla, vagas) values ('Feira de Santana','FS',10);  
  insert into campus (nome, sigla, vagas) values ('Jequi�','JEQ',9); 
  
insert into setor (nome, sigla, campus_id) values ('Auditoria Interna','AUD',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Dir. Administra��o/Brumado','DAP/BRU',(select id from campus where sigla = 'BRU'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Euclides da Cunha','DE/EC',(select id from campus where sigla = 'EC'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Movimenta��o de Pessoal','DEMP/DGP',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Adm/Barreiras','DEPAD/BAR',(select id from campus where sigla = 'BAR'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Adm/Cama�ari','DEPAD/CAM',(select id from campus where sigla = 'CAM'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Adm/Eun�polis','DEPAD/EUN',(select id from campus where sigla = 'EUN'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Adm/Paulo Afonso','DEPAD/PA',(select id from campus where sigla = 'PA'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Adm/Porto Seguro','DEPAD/PS',(select id from campus where sigla = 'PS'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Adm/Santo Amaro','DEPAD/SAM',(select id from campus where sigla = 'SAM'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Adm/Sim�es Filho','DEPAD/SF',(select id from campus where sigla = 'SF'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Adm/Valen�a','DEPAD/VAL',(select id from campus where sigla = 'VAL'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Adm/Vit�ria da Conquista','DEPAD/VC',(select id from campus where sigla = 'VC'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Administra��o/Juazeiro','DEPAT/JUA',(select id from campus where sigla = 'JUA'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Qualidade de Vida','DEQUAV/DGP',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Barreiras','DG/BAR',(select id from campus where sigla = 'BAR'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Brumado','DG/BRU',(select id from campus where sigla = 'BRU'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Cama�ari','DG/CAM',(select id from campus where sigla = 'CAM'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Euclides da Cunha','DG/EC',(select id from campus where sigla = 'EC'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Eun�polis','DG/EUN',(select id from campus where sigla = 'EUN'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Feira de Santana','DG/FS',(select id from campus where sigla = 'FS'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Ilh�us','DG/ILH',(select id from campus where sigla = 'ILH'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Irec�','DG/IRE',(select id from campus where sigla = 'IRE'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Jacobina','DG/JAC',(select id from campus where sigla = 'JAC'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Jequi�','DG/JEQ',(select id from campus where sigla = 'JEQ'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Juazeiro','DG/JUA',(select id from campus where sigla = 'JUA'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Lauro de Freitas','DG/LF',(select id from campus where sigla = 'LF'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Paulo Afonso','DG/PA',(select id from campus where sigla = 'PA'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Polo de Inova��o Salvador','DG/PIS',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Gest�o de Pessoas','DGP/REI',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Porto Seguro','DG/PS',(select id from campus where sigla = 'PS'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Santo Ant�nio de Jesus','DG/SAJ',(select id from campus where sigla = 'SAJ'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Santo Amaro','DG/SAM',(select id from campus where sigla = 'SAM'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Seabra','DG/SEA',(select id from campus where sigla = 'SEA'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Sim�es Filho','DG/SF',(select id from campus where sigla = 'SF'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Salvador','DG/SSA',(select id from campus where sigla = 'SSA'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Gest�o de TI','DGTI/REI',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Ubaitaba','DG/UBA',(select id from campus where sigla = 'UBA'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Valen�a','DG/VAL',(select id from campus where sigla = 'VAL'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Geral/Vit�ria da Conquista','DG/VC',(select id from campus where sigla = 'VC'));
insert into setor (nome, sigla, campus_id) values ('N�cleo Avan�ado de Dias D''�vila/Cama�ari','DIAS/DDV',(select id from campus where sigla = 'CAM'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Pesquisa, P�s-Gradua��o e Inova��o','DPGI/SSA',(select id from campus where sigla = 'SSA'));
insert into setor (nome, sigla, campus_id) values ('Dir de Administra��o/Lauro de Freitas','DA/LF',(select id from campus where sigla = 'LF'));
insert into setor (nome, sigla, campus_id) values ('Diretoria Acad�mica/Seabra','DIRAC/SEA',(select id from campus where sigla = 'SEA'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Barreiras','DIREN/BAR',(select id from campus where sigla = 'BAR'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Brumado','DIREN/BRU',(select id from campus where sigla = 'BRU'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Cama�ari','DIREN/CAM',(select id from campus where sigla = 'CAM'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Eun�polis','DIREN/EUN',(select id from campus where sigla = 'EUN'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Feira de Santana','DIREN/FS',(select id from campus where sigla = 'FS'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Ilh�us','DIREN/ILH',(select id from campus where sigla = 'ILH'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Irec�','DIREN/IRE',(select id from campus where sigla = 'IRE'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Jacobina','DIREN/JAC',(select id from campus where sigla = 'JAC'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Jequi�','DIREN/JEQ',(select id from campus where sigla = 'JEQ'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Juazeiro','DIREN/JUA',(select id from campus where sigla = 'JUA'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Paulo Afonso','DIREN/PA',(select id from campus where sigla = 'PA'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Porto Seguro','DIREN/PS',(select id from campus where sigla = 'PS'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Santo Amaro','DIREN/SAM',(select id from campus where sigla = 'SAM'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Sim�es Filho','DIREN/SF',(select id from campus where sigla = 'SF'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Salvador','DIREN/SSA',(select id from campus where sigla = 'SSA'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Valen�a','DIREN/VAL',(select id from campus where sigla = 'VAL'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Ensino/Vit�ria da Conquista','DIREN/VC',(select id from campus where sigla = 'VC'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Barreiras','GAB/BAR',(select id from campus where sigla = 'BAR'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Cama�ari','GAB/CAM',(select id from campus where sigla = 'CAM'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Eun�polis','GAB/EUN',(select id from campus where sigla = 'EUN'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Feira de Santana','GAB/FS',(select id from campus where sigla = 'FS'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Ilh�us','GAB/ILH',(select id from campus where sigla = 'ILH'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Irec�','GAB/IRE',(select id from campus where sigla = 'IRE'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Jacobina','GAB/JAC',(select id from campus where sigla = 'JAC'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Jequi�','GAB/JEQ',(select id from campus where sigla = 'JEQ'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Paulo Afonso','GAB/PA',(select id from campus where sigla = 'PA'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Porto Seguro','GAB/PS',(select id from campus where sigla = 'PS'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Santo Amaro','GAB/SAM',(select id from campus where sigla = 'SAM'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Seabra','GAB/SEA',(select id from campus where sigla = 'SEA'));
insert into setor (nome, sigla, campus_id) values ('Dir. Administra��o/Feira de Santana','DAP/FS',(select id from campus where sigla = 'FS'));
insert into setor (nome, sigla, campus_id) values ('Dir. Administra��o/Ilh�us','DAP/ILH',(select id from campus where sigla = 'ILH'));
insert into setor (nome, sigla, campus_id) values ('Dir. Administra��o/Irec�','DAP/IRE',(select id from campus where sigla = 'IRE'));
insert into setor (nome, sigla, campus_id) values ('Dir. Administra��o/Jacobina','DAP/JAC',(select id from campus where sigla = 'JAC'));
insert into setor (nome, sigla, campus_id) values ('Dir. Administra��o/Jequi�','DAP/JEQ',(select id from campus where sigla = 'JEQ'));
insert into setor (nome, sigla, campus_id) values ('Dir. Administra��o/Seabra','DAP/SEA',(select id from campus where sigla = 'SEA'));
insert into setor (nome, sigla, campus_id) values ('Diretoria de Administra��o/Salvador','DAP/SSA',(select id from campus where sigla = 'SSA'));
insert into setor (nome, sigla, campus_id) values ('Depart. de Administra��o e Pagamento','DEAP/DGP',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Departamento de Comunica��o Social','DECOM/REI',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Sim�es Filho','GAB/SF',(select id from campus where sigla = 'SF'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Salvador','GAB/SSA',(select id from campus where sigla = 'SSA'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Valen�a','GAB/VAL',(select id from campus where sigla = 'VAL'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Vit�ria da Conquista','GAB/VC',(select id from campus where sigla = 'VC'));
insert into setor (nome, sigla, campus_id) values ('Pr�-Reitoria de Adm. e Planejamento','PROAP',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Pr�-Reitoria de Infraestrutura','PRODIN',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Pr�-Reitoria de Ensino','PROEN',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Pr�-Reitoria de Extens�o','PROEX',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Procuradoria Jur�dica','PROJUR',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Pr�-Reitoria de P�s-Gradua��o e Pesquisa','PRPGI',(select id from campus where sigla = 'REI'));
insert into setor (nome, sigla, campus_id) values ('Gabinete/Reitoria','GAB/REI',(select id from campus where sigla = 'REI'));

  
insert into status (nome) values ('NOVO'), ('ENVIADO CHEFIA'), ('AUTORIZADO CHEFIA'), ('N�O AUTORIZADO PELA CHEFIA'),('HOMOLOGADO'), ('INDEFERIDO'),('CANCELADO');