-- MySQL Script generated by MySQL Workbench
-- Sat Sep 30 10:36:48 2017
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `perfil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `perfil` ;

CREATE TABLE IF NOT EXISTS `perfil` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user` ;

CREATE TABLE IF NOT EXISTS `user` (
  `username` VARCHAR(16) NOT NULL,
  `email` VARCHAR(255) NULL,
  `password` VARCHAR(32) NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `active` TINYINT NOT NULL DEFAULT 1,
  `admin` TINYINT NULL DEFAULT 0,
  `id` INT NOT NULL AUTO_INCREMENT,
  `ldap` TINYINT NOT NULL DEFAULT 1,
  `data_ultimo_acesso` DATETIME NULL,
  `perfil_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_user_perfil1`
    FOREIGN KEY (`perfil_id`)
    REFERENCES `perfil` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE INDEX `fk_user_perfil1_idx` ON `user` (`perfil_id` ASC);


-- -----------------------------------------------------
-- Table `campus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `campus` ;

CREATE TABLE IF NOT EXISTS `campus` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(200) NOT NULL,
  `sigla` VARCHAR(45) NOT NULL,
  `vagas` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `setor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `setor` ;

CREATE TABLE IF NOT EXISTS `setor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(200) NOT NULL,
  `sigla` VARCHAR(45) NOT NULL,
  `campus_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_setor_campus1`
    FOREIGN KEY (`campus_id`)
    REFERENCES `campus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_setor_campus1_idx` ON `setor` (`campus_id` ASC);


-- -----------------------------------------------------
-- Table `servidor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `servidor` ;

CREATE TABLE IF NOT EXISTS `servidor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(250) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  `email_institucional` VARCHAR(255) NULL,
  `siape` VARCHAR(10) NULL,
  `email_pessoal` VARCHAR(255) NULL,
  `sexo` CHAR NOT NULL,
  `cpf` VARCHAR(15) NOT NULL,
  `user_id` INT NULL,
  `setor_id` INT NULL,
  `is_diretor_campus` TINYINT NOT NULL DEFAULT 0,
  `is_chefe_delegacao` TINYINT NOT NULL DEFAULT 0,
  `hospedagem` TINYINT NULL,
  `qtd_acompanhantes_adultos` INT NULL,
  `qtd_acompanhantes_criancas` INT NULL,
  `plano_de_saude` VARCHAR(200) NULL,
  `alergias` VARCHAR(2000) NULL,
  `doencas` VARCHAR(2000) NULL,
  `nome_contato_emergencia` VARCHAR(200) NULL,
  `telefone_contato_emergencia` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_servidor_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_servidor_setor1`
    FOREIGN KEY (`setor_id`)
    REFERENCES `setor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_servidor_user_idx` ON `servidor` (`user_id` ASC);

CREATE INDEX `fk_servidor_setor1_idx` ON `servidor` (`setor_id` ASC);


-- -----------------------------------------------------
-- Table `tipo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tipo` ;

CREATE TABLE IF NOT EXISTS `tipo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `modalidade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `modalidade` ;

CREATE TABLE IF NOT EXISTS `modalidade` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `ativa` TINYINT NOT NULL DEFAULT 1,
  `tipo_id` INT NOT NULL,
  `qtd_titular` INT NOT NULL,
  `qtd_reserva` INT NOT NULL,
  `sexo` VARCHAR(45) NOT NULL,
  `vaga_por_campus` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_modalidade_tipo1`
    FOREIGN KEY (`tipo_id`)
    REFERENCES `tipo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_modalidade_tipo1_idx` ON `modalidade` (`tipo_id` ASC);


-- -----------------------------------------------------
-- Table `equipe`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `equipe` ;

CREATE TABLE IF NOT EXISTS `equipe` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `modalidade` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inscricao`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inscricao` ;

CREATE TABLE IF NOT EXISTS `inscricao` (
  `servidor_id` INT NOT NULL,
  `modalidade_id` INT NOT NULL,
  `data` DATETIME NOT NULL,
  `atestado` BLOB NULL,
  `equipe_id` INT NULL,
  `time_avulso` TINYINT NULL,
  `titular` TINYINT NULL,
  `status_id` INT NOT NULL,
  PRIMARY KEY (`servidor_id`, `modalidade_id`),
  CONSTRAINT `fk_servidor_has_modalidade_servidor1`
    FOREIGN KEY (`servidor_id`)
    REFERENCES `servidor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_servidor_has_modalidade_modalidade1`
    FOREIGN KEY (`modalidade_id`)
    REFERENCES `modalidade` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_inscricao_equipe1`
    FOREIGN KEY (`equipe_id`)
    REFERENCES `equipe` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_inscricao_status1`
    FOREIGN KEY (`status_id`)
    REFERENCES `status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_servidor_has_modalidade_modalidade1_idx` ON `inscricao` (`modalidade_id` ASC);

CREATE INDEX `fk_servidor_has_modalidade_servidor1_idx` ON `inscricao` (`servidor_id` ASC);

CREATE INDEX `fk_inscricao_equipe1_idx` ON `inscricao` (`equipe_id` ASC);

CREATE INDEX `fk_inscricao_status1_idx` ON `inscricao`(`status_id` ASC);

-- -----------------------------------------------------
-- Table `status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `status` ;

CREATE TABLE IF NOT EXISTS `status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `calendario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calendario` ;

CREATE TABLE IF NOT EXISTS `calendario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `data` DATE NOT NULL,
  `hora_inicio` TIME NOT NULL,
  `hora_fim` TIME NULL,
  `local` VARCHAR(100) NOT NULL,
  `descricao` VARCHAR(600) NULL,
  `modalidade_id` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_calendario_modalidade1`
    FOREIGN KEY (`modalidade_id`)
    REFERENCES `modalidade` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_calendario_modalidade1_idx` ON `calendario` (`modalidade_id` ASC);


-- -----------------------------------------------------
-- Table `tipo_doc`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tipo_doc` ;

CREATE TABLE IF NOT EXISTS `tipo_doc` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `docs` ;

CREATE TABLE IF NOT EXISTS `docs` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `documento` BLOB NOT NULL,
  `data` DATETIME NOT NULL,
  `nome_arquivo` VARCHAR(100) NOT NULL,
  `servidor_id` INT NOT NULL,
  `tipo_doc_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_docs_servidor1`
    FOREIGN KEY (`servidor_id`)
    REFERENCES `servidor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_docs_tipo_doc1`
    FOREIGN KEY (`tipo_doc_id`)
    REFERENCES `tipo_doc` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_docs_servidor1_idx` ON `docs` (`servidor_id` ASC);

CREATE INDEX `fk_docs_tipo_doc1_idx` ON `docs` (`tipo_doc_id` ASC);


-- -----------------------------------------------------
-- Table `log`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `log` ;

CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `id_tabela` int(11) NOT NULL,
  `acao` varchar(45) NOT NULL,
  `nome_tabela` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


CREATE TABLE IF NOT EXISTS `campus_modalidade` (
  `campus_id` INT NOT NULL,
  `modalidade_id` INT NOT NULL,
  `vagas` INT NOT NULL,
  PRIMARY KEY (`campus_id`, `modalidade_id`),
  INDEX `fk_campus_has_modalidade_modalidade1_idx` (`modalidade_id` ASC),
  INDEX `fk_campus_has_modalidade_campus1_idx` (`campus_id` ASC),
  CONSTRAINT `fk_campus_has_modalidade_campus1`
    FOREIGN KEY (`campus_id`)
    REFERENCES `campus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_campus_has_modalidade_modalidade1`
    FOREIGN KEY (`modalidade_id`)
    REFERENCES `modalidade` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
