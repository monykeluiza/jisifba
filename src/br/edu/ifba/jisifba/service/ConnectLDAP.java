package br.edu.ifba.jisifba.service;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import br.edu.ifba.jisifba.persist.entities.WebUser;


public class ConnectLDAP {

	private String ldapProviderURL = "ldap://aprendiz.intranet.cefetba.br:389";
	private String userName = null;
	private String userPassword = null;

	public WebUser getUser() {
		
		
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, userName + "@intranet.cefetba.br");
		env.put(Context.SECURITY_CREDENTIALS, userPassword);

		env.put(Context.PROVIDER_URL, ldapProviderURL);

		try {
			LdapContext ctx = new InitialLdapContext(env, null);
			SearchControls searchCtls = new SearchControls();
			String returnedAtts[] = { "sn", "givenName", "ou"};
			searchCtls.setReturningAttributes(returnedAtts);
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			String searchFilter = "(&(objectClass=*)(samaccountname=" + userName + "))";

			String searchBase = "DC=intranet,DC=cefetba,DC=br";

			NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

			if (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attributes = ctx.getAttributes(sr.getName() + ",dc=intranet,dc=cefetba,dc=br");

				WebUser user = new WebUser();
				Attribute attr = attributes.get("cn");
				user.setCn((String) attr.get());

				attr = attributes.get("displayName");
				user.setDisplayName((String) attr.get());
				ctx.close();
				return user;
			} else {
				ctx.close();
				return null;
			}

		} catch (NamingException e) {
			e.printStackTrace();
			return null;
		}

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getLdapProviderURL() {
		return ldapProviderURL;
	}
}