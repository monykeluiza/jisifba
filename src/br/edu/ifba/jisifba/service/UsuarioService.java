package br.edu.ifba.jisifba.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.edu.ifba.jisifba.persist.entities.Log;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.persist.interfaces.IUsuarioDao;

@Stateless
public class UsuarioService extends LogService {
	
	@Inject
	private IUsuarioDao dao;
	
	public List<Usuario> findAll() {
		return dao.pesquisarTodos();
	}
	
	public Usuario insert(Usuario usuario, Usuario usuarioLogado) {
		Usuario result = dao.salvar(usuario);
		Log log = createLog(ACAO_INSERT, result.getId(), usuarioLogado, "User");
		salvar(log);
		return result;
		
	}
	
	public Usuario update(Usuario usuario, Usuario usuarioLogado) {
		Usuario result = dao.atualizar(usuario);
		Log log = createLog(ACAO_UPDATE, result.getId(), usuarioLogado, "User");
		salvar(log);
		return result;
	}
	
	public Usuario findById(Usuario usuario) {
		return dao.pesquisarPorId(usuario);
	}

}
