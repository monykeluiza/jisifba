package br.edu.ifba.jisifba.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Docs;
import br.edu.ifba.jisifba.persist.entities.Log;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.entities.TipoDoc;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.persist.interfaces.IServidorDao;

@Stateless
public class ServidorService extends LogService {
	
	@Inject
	private IServidorDao dao;
	
	public List<Servidor> findAll() {
		return dao.findAll();
	}
	
	public Servidor insert(Servidor servidor, Usuario usuarioLogado) {
		if (validateDocumentos(servidor)) {
			servidor.setIsChefeDelegacao(false);
			servidor.setIsDiretorCampus(false);
			servidor.setIsDiretorCampus(false);
			Servidor result = dao.insert(servidor);
			Log log = createLog(ACAO_INSERT, result.getId(), usuarioLogado, "Servidor");
			salvar(log);
			return result;
		} else {
			return null;
		}
	}
	
	public Servidor update(Servidor servidor, Usuario usuarioLogado) {
		if (validateDocumentos(servidor)) {
			Servidor result = dao.update(servidor);
			Log log = createLog(ACAO_UPDATE, result.getId(), usuarioLogado, "Servidor");
			salvar(log);
			return result;
		} else {
			return null;
		}
	}
	
	public List<Servidor> findByCampusSolicitante(Campus campus) {
		return dao.findByCampusSolicitante(campus);
	}
	
	public List<Servidor> findSolicitantes() {
		return dao.findSolicitantes();
		
	}
	
	public List<Servidor> findHomologados() {
		return dao.findHomologados();
	}
	
	public List<Servidor> findByCampusHomologados(Campus campus) {
		return dao.findByCampusHomologados(campus);
	}
	
	private boolean validateDocumentos(Servidor servidor) {
		List<Docs> documentos = servidor.getDocsList();
		boolean temFoto = false;
		boolean temDocumento = false;
		boolean temAtestado = false;
		for (Docs docs : documentos) {
			if (docs.getTipoDocId().getId().equals(TipoDoc.DOCUMENTO)) {
				temDocumento = true;
			}
			if (docs.getTipoDocId().getId().equals(TipoDoc.ATESTADO)) {
				temAtestado = true;
			}
			if (docs.getTipoDocId().getId().equals(TipoDoc.FOTO)) {
				temFoto = true;
			}
		}
//		return (temFoto && temDocumento && temAtestado);
		return (temFoto && temDocumento);
	}
	
	public void delete(Servidor servidor, Usuario usuarioLogado) {
		dao.delete(servidor);
		Log log = createLog(ACAO_DELETE, servidor.getId(), usuarioLogado, "Servidor");
		salvar(log);
	}
	
	public Servidor findBySiapeCpf(Servidor servidor) {
		return dao.findBySiapeCpf(servidor);
	}
	
	public Servidor findBySiape(Servidor servidor) {
		return dao.findBySiape(servidor);
	}
	
	public Servidor findById(Servidor servidor) {
		return dao.findById(servidor);
	}

}
