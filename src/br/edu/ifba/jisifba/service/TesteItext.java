package br.edu.ifba.jisifba.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class TesteItext {
	
    /** The original PDF file. */
    public static final String COVER
        = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\modelos\\Crach� montando amarelo_servidor_org.pdf";
    /** The original PDF file. */
    public static final String SRC
        = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\modelos\\Crach� montando amarelo_servidor_org.pdf";
 
    /** The resulting PDF file. */
    public static final String DEST
        = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\modelos\\teste.pdf";
    
    public static final String IMG1 = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\branco.jpg";
    
    public static final String IM21 = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\roxo.jpg";
 
    public void manipulatePdf(String src, String dest)
        throws IOException, DocumentException {
        
    	 Document document = new Document();
         PdfWriter.getInstance(document, new FileOutputStream(dest));
         document.open();
         PdfPTable table = new PdfPTable(2);
         table.setTotalWidth(540);
         table.setLockedWidth(true);
         
         PdfPCell cell = new PdfPCell();
         
         Font font = new Font(FontFamily.HELVETICA, 12, Font.NORMAL, GrayColor.BLACK);
         
         Font fontNomeCampus = new Font(FontFamily.HELVETICA, 12, Font.BOLD, GrayColor.BLACK);
         Paragraph campus = new Paragraph("Campus", fontNomeCampus);
         cell.setPaddingTop(170);
         cell.setPaddingLeft(30);
         cell.addElement(campus);
         
         Paragraph nomeCampus = new Paragraph("Santo Ant�nio de Jesus", font);
         cell.addElement(nomeCampus);
         
         Paragraph siape = new Paragraph("SIAPE", fontNomeCampus);
         cell.addElement(siape);
         
         Paragraph numSiape = new Paragraph("2185271", font);
         cell.addElement(numSiape);
         
         Font fontMaior = new Font(FontFamily.HELVETICA, 16, Font.BOLD, GrayColor.BLACK);
         Paragraph nome = new Paragraph("MONIQUE DANTAS", fontMaior);
         nome.setAlignment(Element.ALIGN_CENTER);
         nome.setSpacingBefore(20);
         cell.addElement(nome);
         
         Paragraph cargo = new Paragraph("Servidor T�cnico", font);
         cargo.setAlignment(Element.ALIGN_CENTER);
         cell.addElement(cargo);
         
         Image image = Image.getInstance(IMG1);
         image.scalePercent(30, 30);
         cell.setCellEvent(new ImageBackgroundEvent(image));
         cell.setFixedHeight(380);
         
         Image foto = Image.getInstance(IM21);
         foto.scaleToFit(75, 140);
         cell.setCellEvent(new FotoQuadrante3Event(foto));
         
         Image foto1 = Image.getInstance(IM21);
         foto1.scalePercent(6, 6);
         cell.setCellEvent(new FotoQuadrante1Event(foto1));
         
         Image foto2 = Image.getInstance(IM21);
         foto2.scalePercent(6, 6);
         cell.setCellEvent(new FotoQuadrante2Event(foto2));
         
         Image foto3 = Image.getInstance(IM21);
         foto3.scalePercent(6, 6);
         cell.setCellEvent(new FotoQuadrante4Event(foto3));
         
         table.addCell(cell);
         table.addCell(cell);
         table.addCell(cell);
         table.addCell(cell);
         document.add(table);
         document.close();
    	
    }
    
    class ImageBackgroundEvent implements PdfPCellEvent {
    	 
        protected Image image;
 
        public ImageBackgroundEvent(Image image) {
            this.image = image;
        }
 
        public void cellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
            try {
                PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
                image.scaleAbsolute(position);
                image.setAbsolutePosition(position.getLeft(), position.getBottom());
                cb.addImage(image);
            } catch (DocumentException e) {
                throw new ExceptionConverter(e);
            }
        }
 
    }
    
    class FotoQuadrante1Event implements PdfPCellEvent {
      	 
        protected Image image;
 
        public FotoQuadrante1Event(Image image) {
            this.image = image;
        }
 
        public void cellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
            try {
                PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
                image.setAbsolutePosition(191.2f, 540);
                cb.addImage(image);
            } catch (DocumentException e) {
                throw new ExceptionConverter(e);
            }
        }
 
    }
    
    class FotoQuadrante2Event implements PdfPCellEvent {
     	 
        protected Image image;
 
        public FotoQuadrante2Event(Image image) {
            this.image = image;
        }
 
        public void cellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
            try {
                PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
                image.setAbsolutePosition(462f, 540);
                cb.addImage(image);
            } catch (DocumentException e) {
                throw new ExceptionConverter(e);
            }
        }
 
    }
    
    class FotoQuadrante3Event implements PdfPCellEvent {
   	 
        protected Image image;
 
        public FotoQuadrante3Event(Image image) {
            this.image = image;
        }
 
        public void cellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
            try {
                PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
                image.setAbsolutePosition(191.2f, 160);
                cb.addImage(image);
            } catch (DocumentException e) {
                throw new ExceptionConverter(e);
            }
        }
 
    }
    
    class FotoQuadrante4Event implements PdfPCellEvent {
      	 
        protected Image image;
 
        public FotoQuadrante4Event(Image image) {
            this.image = image;
        }
 
        public void cellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
            try {
                PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
                image.setAbsolutePosition(462f, 160);
                cb.addImage(image);
            } catch (DocumentException e) {
                throw new ExceptionConverter(e);
            }
        }
 
    }
 
    public static void main(String[] args)
        throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        String regulamento = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\pages\\regulamento\\Orientacoes_JISIFBA_2017.pdf";
        new TesteItext().manipulatePdf(regulamento, DEST);
    }
}