package br.edu.ifba.jisifba.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Inscricao;
import br.edu.ifba.jisifba.persist.entities.Log;
import br.edu.ifba.jisifba.persist.entities.Modalidade;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.entities.Status;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.persist.interfaces.IModalidadeDao;

@Stateless
public class ModalidadeService extends LogService {
	
	@Inject
	private IModalidadeDao dao;
	
	@Inject
	private InscricaoService inscricaoService;
	
	public List<Modalidade> findAll() {
		return dao.findAll();
	}
	
	public List<Modalidade> findSetandoAsVagas(Campus campus) {
		List<Modalidade> lista = dao.findAll();
		for (Modalidade modalidade : lista) {
			modalidade = findSetandoAsVagas(campus, modalidade);
		}
		
		return lista;
	}
	
	public Modalidade findSetandoAsVagas(Campus campus, Modalidade modalidade) {
		List<Inscricao> inscricoeDoCampus = inscricaoService.findByCampusModalidadeStatuSolicitacoes(campus, modalidade);
		modalidade.setTotalInscricoesDoCampus(inscricoeDoCampus.size());
		List<Inscricao> inscricoeHomologadasDoCampus = inscricaoService.findByCampusModalidadeStatus(campus, modalidade, new Status(Status.HOMOLOGADO));
		modalidade.setTotalInscricoesHomologadasDoCampus(inscricoeHomologadasDoCampus.size());
		return modalidade;
	}
	
	public List<Modalidade> findSetandoAsVagas() {
		List<Modalidade> lista = dao.findAll();
		for (Modalidade modalidade : lista) {
			List<Inscricao> inscricoes = inscricaoService.findByModalidade(modalidade);
			modalidade.setTotalInscricoes(inscricoes.size());
			List<Inscricao> inscricoeHomologadas = new ArrayList<Inscricao>();
			for (Inscricao i : inscricoes) {
				if (i.getStatusId().getId().equals(Status.HOMOLOGADO)) {
					inscricoeHomologadas.add(i);
				}
			}
			modalidade.setTotalInscricoesHomologadas(inscricoeHomologadas.size());
		}
		return lista;
	}
	
	public Modalidade insert(Modalidade modalidade, Usuario usuarioLogado) {
		Modalidade result = dao.insert(modalidade);
		Log log = createLog(ACAO_INSERT, result.getId(), usuarioLogado, "Modalidade");
		salvar(log);
		return result;
		
	}
	
	public Modalidade update(Modalidade modalidade, Usuario usuarioLogado) {
		Modalidade result = dao.update(modalidade);
		Log log = createLog(ACAO_UPDATE, result.getId(), usuarioLogado, "Modalidade");
		salvar(log);
		return result;
	}
	
	public void delete(Modalidade modalidade, Usuario usuarioLogado) {
		dao.delete(modalidade);
		Log log = createLog(ACAO_DELETE, modalidade.getId(), usuarioLogado, "Modalidade");
		salvar(log);
	}
	
	public Modalidade findById(Modalidade modalidade) {
		return dao.findById(modalidade);
	}
	
	public List<Modalidade> findAtivas(Servidor servidor) {
		return dao.findAtivas(servidor);
	}
	
}
