package br.edu.ifba.jisifba.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Inscricao;
import br.edu.ifba.jisifba.persist.entities.Log;
import br.edu.ifba.jisifba.persist.entities.Status;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.persist.interfaces.ICampusDao;

@Stateless
public class CampusService extends LogService {
	
	@Inject
	private ICampusDao dao;
	
	@Inject
	private InscricaoService inscricaoService;
	
	@Inject
	private ServidorService servidorService;
	
	public List<Campus> findAll() {
		return dao.findAll();
	}
	
	public Campus insert(Campus campus, Usuario usuarioLogado) {
		Campus result = dao.insert(campus);
		Log log = createLog(ACAO_INSERT, result.getId(), usuarioLogado, "Campus");
		salvar(log);
		return result;
		
	}
	
	public Campus update(Campus campus, Usuario usuarioLogado) {
		Campus result = dao.update(campus);
		Log log = createLog(ACAO_UPDATE, result.getId(), usuarioLogado, "Campus");
		salvar(log);
		return result;
	}
	
	public void delete(Campus campus, Usuario usuarioLogado) {
		dao.delete(campus);
		Log log = createLog(ACAO_DELETE, campus.getId(), usuarioLogado, "Campus");
		salvar(log);
	}
	
	public List<Campus> findSetandoVagas() {
		List<Campus> lista =  dao.findAll();
		for (Campus campus : lista) {
			List<Inscricao> solicitacoes = inscricaoService.findByCampusStatuSolicitacoes(campus);
			campus.setSolicitacoes(solicitacoes.size());
			List<Inscricao> homologacoes = inscricaoService.findByCampusStatus(campus, new Status(Status.HOMOLOGADO));
			campus.setHomologacoes(homologacoes.size());
			campus.setTotalServidoresSolicitantes(servidorService.findByCampusSolicitante(campus).size());
			campus.setTotalServidoresHomologados(servidorService.findByCampusHomologados(campus).size());
		}
		return lista;
	}
	
	public Campus findById(Campus campus) {
		return dao.findById(campus);
	}

}
