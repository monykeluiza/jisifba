package br.edu.ifba.jisifba.service;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Inscricao;
import br.edu.ifba.jisifba.persist.entities.InscricaoPK;
import br.edu.ifba.jisifba.persist.entities.Log;
import br.edu.ifba.jisifba.persist.entities.Modalidade;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.entities.Status;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.persist.interfaces.IInscricaoDao;

@Stateless
public class InscricaoService extends LogService {
	
	@Inject
	private IInscricaoDao dao;
	
	@Inject
	private EquipeService equipeService;
	
	@Inject
	private ServidorService servidorService;
	
	private int maxInd = 2;
	private int maxCol = 1;
	
	public List<Inscricao> findAll() {
		return dao.findAll();
	}
	
	public List<Inscricao> findByServidor(Servidor servidor) {
		return dao.findByServidor(servidor);
	}
	
	public List<Inscricao> findByCampus(Campus campus) {
		return dao.findByCampus(campus);
	}
	
	public Inscricao insert(Inscricao inscricao, Usuario usuarioLogado) throws Exception {
		InscricaoPK pk = new InscricaoPK(usuarioLogado.getServidorList().get(0).getId(), inscricao.getModalidade().getId());
		inscricao.setInscricaoPK(pk);
		if (validateInsert(inscricao)) {
			inscricao.setData(new Date());
			if (inscricao.getModalidade().getTipoId().getId().equals(1)) {
				inscricao.setTitular(true);
			}
			if (inscricao.getModalidade().getTipoId().getId().equals(2) && !inscricao.getTimeAvulso()) {
				inscricao.getEquipeId().setModalidade(inscricao.getModalidade().getNome());
				inscricao.setEquipeId(equipeService.insert(inscricao.getEquipeId(), usuarioLogado));
			}
			if (inscricao.getEquipeId().getId() == null) {
				inscricao.setEquipeId(null);
			}
			inscricao.setStatusId(new Status(1));
			Inscricao result = dao.insert(inscricao);
			Log log = createLog(ACAO_INSERT, result.getServidor().getId(), usuarioLogado, "Inscricao");
			salvar(log);
			return result;
		} else {
			return null;
		}
	}
	
	public String validarHomologacao(Inscricao inscricao, Servidor servidorAutorHomologacao) {
		int totalServidoresModadlidade = 0;
		int totalServidoresInscritos = 0;
//		List<Inscricao> inscricoesCampus = findByCampus(servidorAutorHomologacao.getSetorId().getCampusId());
//		for (Inscricao insc : inscricoesCampus) {
//			if (insc.getStatusId().getId().equals(Status.HOMOLOGADO)) {
//				++totalServidoresInscritos;
//				if (inscricao.getModalidade().getId().equals(insc.getModalidade().getId())) {
//					++totalServidoresModadlidade;
//				}
//			}
//		}
		List<Servidor> servidores = servidorService.findByCampusHomologados(servidorAutorHomologacao.getSetorId().getCampusId());
		List<Inscricao> inscricoesCampus = findByCampusStatus(servidorAutorHomologacao.getSetorId().getCampusId(), new Status(Status.HOMOLOGADO));
		totalServidoresInscritos = servidores.size();
		boolean servidorJaTemHomologacao = false;
		
		for (Servidor serv : servidores) {
			if (serv.getId().equals(inscricao.getServidor().getId())) {
				servidorJaTemHomologacao = true;
			}
		}
		
		for (Inscricao insc : inscricoesCampus) {
			if (inscricao.getModalidade().getId().equals(insc.getModalidade().getId())) {
				++totalServidoresModadlidade;
			}
		}
		
		
		if (totalServidoresInscritos >= servidorAutorHomologacao.getSetorId().getCampusId().getVagas() && !servidorJaTemHomologacao) {
			return "Homologa��o n�o realizada. Campus atingiu total de vagas reservadas.";
		}
		
		if (totalServidoresModadlidade >= inscricao.getModalidade().getVagaPorCampus()) {
			return "Homologa��o n�o realizada. Campus atingiu total de vagas reservadas para esta modalidade.";
		}
		
		return null;
	}
	
	public boolean validate(Servidor servidor) {
		if (!validadeDados(servidor)) {
			return false;
		}
		List<Inscricao> listaInscricoesDoServidor = dao.findByServidorAtivas(servidor);
		int individual = getQtdInscIndividuais(listaInscricoesDoServidor);
		int coletivo = getQtdInscColetivo(listaInscricoesDoServidor);
		if (individual >= maxInd && coletivo >= maxCol) {
			return false;
		}
		return true;
	}
	
	private int getQtdInscIndividuais(List<Inscricao> listaInscricoesDoServidor) {
		int individual = 0;
		if (getQtdInscricoesAtletismo(listaInscricoesDoServidor) > 0) {
			++individual;
		}
		if (getQtdInscricoesNatacao(listaInscricoesDoServidor) > 0) {
			++individual;
		}
		
		List<Inscricao> inscricoesFinais = new ArrayList<Inscricao>();
		inscricoesFinais.addAll(listaInscricoesDoServidor);
		for (Inscricao insc : listaInscricoesDoServidor) {
			if (insc.getModalidade().getNome().contains("NATA��O") || insc.getModalidade().getNome().contains("ATLETISMO")) {
				inscricoesFinais.remove(insc);
			}
		}
		
		for (Inscricao insc : inscricoesFinais) {
			if (insc.getModalidade().getTipoId().getId() == 1) {
				if (!insc.getStatusId().getId().equals(7)) {
					++individual;
				}
			} 
		}
		
		return individual;
	}
	
	private int getQtdInscColetivo(List<Inscricao> listaInscricoesDoServidor) {
		int coletivo = 0;
		for (Inscricao insc : listaInscricoesDoServidor) {
			if (insc.getModalidade().getTipoId().getId() == 2) {
				if (!insc.getStatusId().getId().equals(7)) {
					++coletivo;
				}
			} 
		}
		return coletivo;
	}
	
	
	private int getQtdInscricoesNatacao(List<Inscricao> listaInscricoesDoServidor) {
		int count = 0;
		for (Inscricao insc : listaInscricoesDoServidor) {
			if (insc.getModalidade().getNome().contains("NATA��O")) {
				if (!insc.getStatusId().getId().equals(Status.CANCELADO)) {
					++count;
				}
			}
		}
		return count;
	}
	
	private int getQtdInscricoesAtletismo(List<Inscricao> listaInscricoesDoServidor) {
		int count = 0;
		for (Inscricao insc : listaInscricoesDoServidor) {
			if (insc.getModalidade().getNome().contains("ATLETISMO")) {
				if (!insc.getStatusId().getId().equals(Status.CANCELADO)) {
					++count;
				}
			}
		}
		return count;
	}
	
	private boolean validadeDados(Servidor servidor) {
		if (servidor.getDocsList() == null || servidor.getDocsList().isEmpty()) {
			return false;
		}
		
		if (servidor.getSetorId() == null || servidor.getSetorId().getId() == null) {
			return false;
		}
		
		if (servidor.getHospedagem() == null) {
			return false;
		}
		
		if (servidor.getPlanoDeSaude() == null) {
			return false;
		}
		if (servidor.getNomeContatoEmergencia() == null) {
			return false;
		}
		if (servidor.getTelefoneContatoEmergencia() == null) {
			return false;
		}
		return true;
	}
	
	public boolean validateInsert(Inscricao inscricao) {	
		List<Inscricao> listaInscricoesDoServidor = dao.findByServidorAtivas(new Servidor(inscricao.getInscricaoPK().getServidorId()));
		boolean temNatacao = false;
		boolean temAtletismo = false;
		for (Inscricao i : listaInscricoesDoServidor) {
			if (!i.getStatusId().getId().equals(Status.CANCELADO) && !i.getStatusId().getId().equals(Status.INDEFERIDO) && !i.getStatusId().getId().equals(Status.NAO_AUTORIZADO_CHEFIA)) {
				if (i.getModalidade().getNome().contains("NATA��O")) {
					temNatacao = true;
				}
				if (i.getModalidade().getNome().contains("ATLETISMO")) {
					temAtletismo = true;
				}
			}
		}
		int individual = getQtdInscIndividuais(listaInscricoesDoServidor);
		int coletivo = getQtdInscColetivo(listaInscricoesDoServidor);
		if (inscricao.getModalidade().getTipoId().getId().equals(1)) {
			if (individual >= maxInd) {
				if (inscricao.getModalidade().getNome().contains("NATA��O") && temNatacao) {
					return true;
				}
				if (inscricao.getModalidade().getNome().contains("ATLETISMO") && temAtletismo) {
					return true;
				}
				return false;
			}
		} else {
			if (coletivo >= maxCol) {
				return false;
			} 
		}
		return true;
	}
	
	public Inscricao update(Inscricao inscricao, Usuario usuarioLogado) {
		if (!inscricao.getStatusId().getId().equals(Status.NOVO) || validateInsert(inscricao)){
			if (inscricao.getEquipeId().getId() == null) {
				inscricao.setEquipeId(null);
			}
			Inscricao result = dao.update(inscricao);
			Log log = createLog(ACAO_UPDATE, result.getServidor().getId(), usuarioLogado, "Inscricao");
			salvar(log);
			return result;
		} else {
			return null;
		}
	}
	
	public void delete(Inscricao inscricao, Usuario usuarioLogado) {
		dao.delete(inscricao);
		Log log = createLog(ACAO_DELETE, inscricao.getServidor().getId(), usuarioLogado, "Inscricao");
		salvar(log);
	}
	
	public Inscricao findById(Inscricao inscricao) {
		return dao.findById(inscricao);
	}
	
	public List<Inscricao> findByCampusModalidade(Campus campus, Modalidade modalidade) {
		return dao.findByCampusModalidade(campus, modalidade);
	}
	
	public List<Inscricao> findByModalidade(Modalidade modalidade) {
		return dao.findByModalidade(modalidade);
	}
	
	public List<Inscricao> findByCampusModalidadeStatus(Campus campus, Modalidade modalidade, Status status) {
		return dao.findByCampusModalidadeStatus(campus, modalidade, status);
	}
	
	public List<Inscricao> findByCampusStatus(Campus campus,Status status) {
		return dao.findByCampusStatus(campus, status);
	}
	
	public List<Inscricao> findByCampusStatuSolicitacoes(Campus campus) {
		return dao.findByCampusStatuSolicitacoes(campus);
	}
	
	public List<Inscricao> findByCampusModalidadeStatuSolicitacoes(Campus campus, Modalidade modalidade) {
		return dao.findByCampusModalidadeStatuSolicitacoes(campus, modalidade);
	}
	
	public List<Inscricao> findByStatus(Status status) {
		return dao.findByStatus(status);
	}
	
	public List<Inscricao> findByTimeAvulso() {
		return dao.findByTimeAvulso();
	}
	
	public List<Inscricao> findByFilter(Inscricao inscricao) {
		return dao.findByFilter(inscricao);
	}
	
	public void enviarEmailMudancaStatus(String emailDestino, String nome, Session mailSession) throws Exception {
		Message msg = new MimeMessage(mailSession);

		msg.setSubject("Jogos dos Servidores 2017 - Atualiza��o de Status de Solicita��o");
		msg.setRecipient(RecipientType.TO, new InternetAddress(emailDestino));

		StringWriter writer = new StringWriter();
		writer.append("<HTML><meta http-equiv=\"Content-Type\"  content=\"text/html charset=ISO-8859-1\" />	<HEAD>		   <style type=\"text/css\">		</style>		</HEAD>		<BODY>			<div>");
		writer.append("Prezado(a) " + nome + ",<br><br>");
		writer.append("O status da sua solicita&ccedil;&atilde;o foi atualizado pelo respons&aacute;vel. Acesse o sistema e verifique as informa&ccedil&otilde;es. ");
		writer.append("<br><br>Lembramos que apenas as modalidades Domin&oacute;, Xadrez, Dama e Jogos Eletr&ocirc;nicos n&atilde;o exigem atestado m&eacute;dico ou termo de responsabilidade. ");
		writer.append("<br><br>Para todas as outras o atestado ou o termo de responsabilidade devidamente preenchido e assinado ser&aacute; exigido. Estes podem ser anexados a qualquer momento. Para isso utilize a op&ccedil;&atilde;o MEUS DADOS.");

		writer.append("<br /><br />Atenciosamente,<br><br> Comiss&atilde;o organizadora dos Jogos<br> <br/> Este &eacute; uma email autom&aacute;tico, favor n&atilde;o responder essa mensagem! </div> </BODY> </HTML>");
		
		BodyPart body = new MimeBodyPart();
		body.setContent(writer.toString(), "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(body);
		msg.setContent(multipart, "text/html");

		Transport.send(msg);
	}
	
	public void enviarEmailSolicitacao(String emailDestino, String nome, Session mailSession) throws Exception {
		Message msg = new MimeMessage(mailSession);

		msg.setSubject("Jogos dos Servidores 2017 - Novo cadastro");
		msg.setRecipient(RecipientType.TO, new InternetAddress(emailDestino));

		StringWriter writer = new StringWriter();
		writer.append("<HTML><meta http-equiv=\"Content-Type\"  content=\"text/html charset=ISO-8859-1\" />	<HEAD>		   <style type=\"text/css\">		</style>		</HEAD>		<BODY>			<div>");
		writer.append("Prezado(a) " + nome + ",<br><br>");
		writer.append("Sua solicita&ccedil;&atilde;o foi criada. Clique no bot&atilde;o ENVIAR para que a mesma seja validada e homologada pelos respons&aacute;veis.");
		writer.append("<br><br>Verifique se n&atilde;o h&aacute; choque de hor&aacute;rio nas modalidades escolhidas acessando o CALEND&Aacute;RIO PRELIMINAR do sistema. ");
		writer.append("<br><br>Voc&ecirc; pode cancelar a solicita&ccedil;&atilde;o a qualquer momento.");
		writer.append("<br><br>Lembramos que apenas as modalidades Domin&oacute;, Xadrez, Dama e Jogos Eletr&ocirc;nicos n&atilde;o exigem atestado m&eacute;dico ou termo de responsabilidade. ");
		writer.append("<br><br>Para todas as outras o atestado ou o termo de responsabilidade devidamente preenchido e assinado ser&aacute; exigido. Estes podem ser anexados a qualquer momento. Para isso utilize a op&ccedil;&atilde;o MEUS DADOS.");
		writer.append("<br /><br />Atenciosamente,<br><br> Comiss&atilde;o organizadora dos Jogos<br> <br/> Este &eacute; uma email autom&aacute;tico, favor n&atilde;o responder essa mensagem! </div> </BODY> </HTML>");
		
		BodyPart body = new MimeBodyPart();
		body.setContent(writer.toString(), "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(body);
		msg.setContent(multipart, "text/html");

		Transport.send(msg);

	}
	
	

}
