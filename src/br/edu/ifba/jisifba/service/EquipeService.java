package br.edu.ifba.jisifba.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.edu.ifba.jisifba.persist.entities.Equipe;
import br.edu.ifba.jisifba.persist.entities.Log;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.persist.interfaces.IEquipeDao;

@Stateless
public class EquipeService extends LogService {
	
	@Inject
	private IEquipeDao dao;
	
	public List<Equipe> findAll() {
		return dao.findAll();
	}
	
	public Equipe insert(Equipe equipe, Usuario usuarioLogado) {
		equipe.setNome(equipe.getNome().toUpperCase());
		Equipe pesquisa = dao.findByNome(equipe);
		if (pesquisa != null && pesquisa.getId() != null) {
				return pesquisa;
		}
		Equipe result = dao.insert(equipe);
		Log log = createLog(ACAO_INSERT, result.getId(), usuarioLogado, "Equipe");
		salvar(log);
		return result;
	}
	
	public Equipe update(Equipe equipe, Usuario usuarioLogado) {
		Equipe result = dao.update(equipe);
		Log log = createLog(ACAO_UPDATE, result.getId(), usuarioLogado, "Equipe");
		salvar(log);
		return result;
	}
	
	public void delete(Equipe equipe, Usuario usuarioLogado) {
		dao.delete(equipe);
		Log log = createLog(ACAO_DELETE, equipe.getId(), usuarioLogado, "Equipe");
		salvar(log);
	}
	
	public Equipe findById(Equipe equipe) {
		return dao.findById(equipe);
	}

}
