package br.edu.ifba.jisifba.service;

import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.edu.ifba.jisifba.persist.entities.Log;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.persist.interfaces.ILogDao;

@Stateless
public class LogService {
	
	public static final String ACAO_INSERT = "INSERT";
	public static final String ACAO_DELETE = "DELETE";
	public static final String ACAO_UPDATE = "UPDATE";
	
	@Inject
	private ILogDao logDao;
	
	private Usuario usuarioLogado;
	
	public void salvar(Log log) {
		logDao.inserir(log);
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}
	
	public Log createLog(String acao, Integer idTabela, Usuario usuarioLogado, String NomeTabela) {
		Log log = new Log();
		log.setAcao(acao);
		log.setData(new Date());
		if (idTabela != null) {
			log.setIdTabela(idTabela);
		}
		log.setIdUsuario(usuarioLogado.getId());
		log.setLogin(usuarioLogado.getUsername());
		log.setNome(usuarioLogado.getUsername());
		log.setNomeTabela(NomeTabela);
		return log;
	}

}
