package br.edu.ifba.jisifba.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import br.edu.ifba.jisifba.persist.entities.Cracha;
import br.edu.ifba.jisifba.persist.entities.Docs;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.entities.TipoDoc;

@Stateless
public class CrachaService {
    public static final String DEST
    = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\modelos\\crachas_ifba.pdf";
    public static final String DEST_DELEGACAO
    = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\modelos\\crachas_delegacao_ifba.pdf";
    public static final String DEST_COMISSAO
    = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\modelos\\crachas_comissao_ifba.pdf";
    public static final String IMG_SERVIDOR_ATLETA = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\branco.jpg";
    public static final String IMG_ORGANIZACAO = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\amarelo.jpg";
    public static final String IMG_DELEGACAO = "C:\\Users\\monyk\\Documents\\DESENVOLVIMENTO\\workspace\\jogos2017\\WebContent\\cracha\\verde.jpg";
    
	private void gerarCracha(List<Cracha> crachas, String dest, BaseColor color) throws IOException, DocumentException {
		 Document document = new Document();
         PdfWriter.getInstance(document, new FileOutputStream(dest));
         document.open();
         PdfPTable table = new PdfPTable(2);
         table.setTotalWidth(540);
         table.setLockedWidth(true);
         
         Font font = new Font(FontFamily.HELVETICA, 12, Font.NORMAL, color);
         Font fontNomeCampus = new Font(FontFamily.HELVETICA, 12, Font.BOLD, color);
         Font fontMaior = new Font(FontFamily.HELVETICA, 16, Font.BOLD, color);
         
         Paragraph campus = new Paragraph("Campus", fontNomeCampus);
         Paragraph siape = new Paragraph("SIAPE", fontNomeCampus);
         
         int quadrante = 1;
         
         for (Cracha cracha : crachas) {
        	 PdfPCell cell = new PdfPCell();
        	 cell.setPaddingTop(170);
        	 cell.setPaddingLeft(30);
        	 cell.addElement(campus);
             Paragraph nomeCampus = new Paragraph(cracha.getCampus(), font);
             cell.addElement(nomeCampus);
             cell.addElement(siape);
             Paragraph numSiape = new Paragraph(cracha.getSiape(), font);
             cell.addElement(numSiape);
             Paragraph nome = new Paragraph(cracha.getNome(), fontMaior);
             nome.setAlignment(Element.ALIGN_CENTER);
             nome.setSpacingBefore(20);
             cell.addElement(nome);
             Paragraph cargo = new Paragraph(cracha.getCargo(), font);
             cargo.setAlignment(Element.ALIGN_CENTER);
             cell.addElement(cargo);
             
             Image image = Image.getInstance(cracha.getImgBackground());
             image.scalePercent(30, 30);
             cell.setCellEvent(new ImageBackgroundEvent(image));
             cell.setFixedHeight(380);
             Image foto = null;
             if (cracha.getFoto() != null) {
            	 foto = Image.getInstance(cracha.getFoto());
             } else {
            	 System.out.println(">>>>> " + cracha.getNome() + " N�O TEM FOTO!!");
            	 foto = Image.getInstance(IMG_SERVIDOR_ATLETA);
             }
             foto.scaleToFit(75, 140);
             boolean somarQuadrante = false;
             if (quadrante == 1) {
            	 cell.setCellEvent(new FotoQuadrante1Event(foto));
            	 somarQuadrante = true;
             }
             if (quadrante == 2) {
            	 cell.setCellEvent(new FotoQuadrante2Event(foto));
            	 somarQuadrante = true;
             }
             if (quadrante == 3) {
            	 cell.setCellEvent(new FotoQuadrante3Event(foto));
            	 somarQuadrante = true;
             }
             if (quadrante == 4) {
            	 cell.setCellEvent(new FotoQuadrante4Event(foto));
            	 quadrante = 1;
             }
             if (somarQuadrante) {
            	 quadrante++;
             }
             table.addCell(cell);
		}
         document.add(table);
         document.close();
	}
	
	
	public void gerarCrachaServidorAtleta(List<Servidor> servidoresHomologados) {
		List<Cracha> crachas = new ArrayList<Cracha>();
		for (Servidor servidor : servidoresHomologados) {
			Cracha cracha = new Cracha();
			cracha.setCampus(servidor.getSetorId().getCampusId().getNome());
			cracha.setCargo("Servidor Atleta");
			List<Docs> docs = servidor.getDocsList();
			for (Docs docs2 : docs) {
				if (docs2.getTipoDocId().getId().equals(TipoDoc.FOTO)) {
					cracha.setFoto(docs2.getDocumento());
					break;
				}
			}
			cracha.setImgBackground(IMG_SERVIDOR_ATLETA);
			cracha.setNome(servidor.getNome().substring(0,servidor.getNome().indexOf(" ")) + " "+ new String(servidor.getNome().substring(servidor.getNome().lastIndexOf(" "))));
			cracha.setSiape(servidor.getSiape());
			crachas.add(cracha);
		}
		
		try {
			gerarCracha(crachas, DEST, GrayColor.BLACK);
		} catch (IOException | DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	public void gerarCrachaServidorOrganizador(List<Servidor> servidoresOrganizador) {
		List<Cracha> crachas = new ArrayList<Cracha>();
		for (Servidor servidor : servidoresOrganizador) {
			Cracha cracha = new Cracha();
			cracha.setCampus(servidor.getSetorId().getCampusId().getNome());
			cracha.setCargo("Servidor Organizador");
			List<Docs> docs = servidor.getDocsList();
			for (Docs docs2 : docs) {
				if (docs2.getTipoDocId().getId().equals(TipoDoc.FOTO)) {
					cracha.setFoto(docs2.getDocumento());
					break;
				}
			}
			cracha.setImgBackground(IMG_ORGANIZACAO);
			cracha.setNome(servidor.getNome().substring(0,servidor.getNome().indexOf(" ")) + " "+ new String(servidor.getNome().substring(servidor.getNome().lastIndexOf(" "))));
			cracha.setSiape(servidor.getSiape());
			crachas.add(cracha);
		}
		
		try {
			gerarCracha(crachas, DEST_COMISSAO, GrayColor.BLACK);
		} catch (IOException | DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void gerarCrachaChefeDelegacao(List<Servidor> chefesDelegacao) {
		List<Cracha> crachas = new ArrayList<Cracha>();
		for (Servidor servidor : chefesDelegacao) {
			Cracha cracha = new Cracha();
			cracha.setCampus(servidor.getSetorId().getCampusId().getNome());
			cracha.setCargo("Servidor chefe de delega��o");
			List<Docs> docs = servidor.getDocsList();
			for (Docs docs2 : docs) {
				if (docs2.getTipoDocId().getId().equals(TipoDoc.FOTO)) {
					cracha.setFoto(docs2.getDocumento());
					break;
				}
			}
			cracha.setImgBackground(IMG_DELEGACAO);
			cracha.setNome(servidor.getNome().substring(0,servidor.getNome().indexOf(" ")) + " "+ new String(servidor.getNome().substring(servidor.getNome().lastIndexOf(" "))));
			cracha.setSiape(servidor.getSiape());
			crachas.add(cracha);
		}
		
		try {
			gerarCracha(crachas, DEST_DELEGACAO, GrayColor.WHITE);
		} catch (IOException | DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	
    class ImageBackgroundEvent implements PdfPCellEvent {
   	 
        protected Image image;
 
        public ImageBackgroundEvent(Image image) {
            this.image = image;
        }
 
        public void cellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
            try {
                PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
                image.scaleAbsolute(position);
                image.setAbsolutePosition(position.getLeft(), position.getBottom());
                cb.addImage(image);
            } catch (DocumentException e) {
                throw new ExceptionConverter(e);
            }
        }
 
    }
    
    class FotoQuadrante1Event implements PdfPCellEvent {
      	 
        protected Image image;
 
        public FotoQuadrante1Event(Image image) {
            this.image = image;
        }
 
        public void cellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
            try {
                PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
                image.setAbsolutePosition(191.2f, 540);
                cb.addImage(image);
            } catch (DocumentException e) {
                throw new ExceptionConverter(e);
            }
        }
 
    }
    
    class FotoQuadrante2Event implements PdfPCellEvent {
     	 
        protected Image image;
 
        public FotoQuadrante2Event(Image image) {
            this.image = image;
        }
 
        public void cellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
            try {
                PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
                image.setAbsolutePosition(462f, 540);
                cb.addImage(image);
            } catch (DocumentException e) {
                throw new ExceptionConverter(e);
            }
        }
 
    }
    
    class FotoQuadrante3Event implements PdfPCellEvent {
   	 
        protected Image image;
 
        public FotoQuadrante3Event(Image image) {
            this.image = image;
        }
 
        public void cellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
            try {
                PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
                image.setAbsolutePosition(191.2f, 160);
                cb.addImage(image);
            } catch (DocumentException e) {
                throw new ExceptionConverter(e);
            }
        }
 
    }
    
    class FotoQuadrante4Event implements PdfPCellEvent {
      	 
        protected Image image;
 
        public FotoQuadrante4Event(Image image) {
            this.image = image;
        }
 
        public void cellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
            try {
                PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
                image.setAbsolutePosition(462f, 160);
                cb.addImage(image);
            } catch (DocumentException e) {
                throw new ExceptionConverter(e);
            }
        }
 
    }

	
	public static void main(String[] args) {
		
	}
}
