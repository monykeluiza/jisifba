package br.edu.ifba.jisifba.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.edu.ifba.jisifba.persist.entities.Log;
import br.edu.ifba.jisifba.persist.entities.Setor;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.persist.interfaces.ISetorDao;

@Stateless
public class SetorService extends LogService {
	
	@Inject
	private ISetorDao dao;
	
	public List<Setor> findAll() {
		return dao.findAll();
	}
	
	public Setor insert(Setor setor, Usuario usuarioLogado) {
		Setor result = dao.insert(setor);
		Log log = createLog(ACAO_INSERT, result.getId(), usuarioLogado, "Setor");
		salvar(log);
		return result;
		
	}
	
	public Setor update(Setor setor, Usuario usuarioLogado) {
		Setor result = dao.update(setor);
		Log log = createLog(ACAO_UPDATE, result.getId(), usuarioLogado, "Setor");
		salvar(log);
		return result;
	}
	
	public void delete(Setor setor, Usuario usuarioLogado) {
		dao.delete(setor);
		Log log = createLog(ACAO_DELETE, setor.getId(), usuarioLogado, "Setor");
		salvar(log);
	}
	
	public Setor findById(Setor setor) {
		return dao.findById(setor);
	}

}
