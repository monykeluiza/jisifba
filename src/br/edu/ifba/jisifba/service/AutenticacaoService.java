
package br.edu.ifba.jisifba.service;

import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.edu.ifba.jisifba.persist.entities.Perfil;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.persist.entities.WebUser;
import br.edu.ifba.jisifba.persist.interfaces.IUsuarioDao;
import br.edu.ifba.jisifba.util.Util;

@Stateless
public class AutenticacaoService {
	
	@Inject
	private IUsuarioDao usuarioDao;
	
	public Usuario autenticar(Usuario usuario) throws Exception {
		Usuario usuarioBanco = usuarioDao.pesquisarPorLoginAtivo(usuario);
			try {
				// LDAP
				if (usuarioBanco != null  && usuarioBanco.getId() != null && usuarioBanco.getLdap()) {
					usuario = autenticarLDAP(usuario);
					usuarioBanco = createOrUpdateUser(usuario, true);
				} else {
					//SEM LDAP
					if (usuarioBanco != null && usuarioBanco.getId() != null &&  !usuarioBanco.getLdap()) {
						usuario = autenticarComum(usuario);
						usuarioBanco = createOrUpdateUser(usuario, false);
					}
					if (usuarioBanco == null || usuarioBanco.getId() == null) {
						usuario = autenticarLDAP(usuario);
						usuarioBanco = createOrUpdateUser(usuario, true);
					}
					
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			
			if (usuarioBanco == null || usuarioBanco.getId() == null) {
				throw new Exception("Usu�rio ou senha n�o conferem!");
			}
			
			return usuarioBanco;
	}
	
	private Usuario createOrUpdateUser(Usuario usuario, boolean isLdap) {
		Usuario usuarioBanco = usuarioDao.pesquisarPorLoginAtivo(usuario);
		if (isLdap) {
			if (usuarioBanco != null) {
				if (usuarioBanco.getDataUltimoAcesso() == null) {
					usuarioBanco.setId(usuarioBanco.getId());
				}
				usuarioBanco.setDataUltimoAcesso(new Date());
				usuarioDao.atualizar(usuarioBanco);
			} else {
				usuarioBanco = new Usuario();
				usuarioBanco.setActive(true);
				usuarioBanco.setCreateTime(new Date());
				usuarioBanco.setUsername(usuario.getUsername());
				usuarioBanco.setDataUltimoAcesso(new Date());
				usuarioBanco.setPerfilId(new Perfil(1));
				usuarioBanco.setLdap(true);
				usuarioBanco.setAdmin(false);
				usuarioDao.salvar(usuarioBanco);
			}
		} else {
			if (usuario.getDataUltimoAcesso() == null) {
				usuario.setId(usuario.getId());
			}
			usuario.setDataUltimoAcesso(new Date());
			usuarioDao.atualizar(usuario);
		}
		return usuarioBanco;
	}
	
	private Usuario autenticarLDAP(Usuario usuario) throws Exception {
		ConnectLDAP conexao = new ConnectLDAP();
		conexao.setUserName(usuario.getUsername());
		conexao.setUserPassword(usuario.getPassword());
		WebUser webuser = conexao.getUser();
		if (webuser == null) {
			throw new Exception("Usu�rio ou senha n�o conferem!");
		}
		return usuario;
	}
	
	private Usuario autenticarComum(Usuario usuario) throws Exception {
		usuario.setPassword(Util.getMD5String(usuario.getPassword()));
		Usuario autenticado = usuarioDao.pesquisarPorLoginSenhaAtivo(usuario);
		if (autenticado == null || autenticado.getId() == null) {
			throw new Exception("Usu�rio ou senha n�o conferem!");
		}
		return autenticado;
	}
}
