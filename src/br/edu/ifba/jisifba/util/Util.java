package br.edu.ifba.jisifba.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

import br.edu.ifba.jisifba.persist.entities.Usuario;

public class Util {
	
	public static String toCammelCase(String nome) {
		try {
			nome = nome.toLowerCase();
			String[] semEspaco = nome.split(" ");
			String retorno = "";
			for (String str : semEspaco) {
				String primeiraLetra = str.substring(0, 1);
				String restoDaPalavra = str.substring(1);
				retorno += primeiraLetra.toUpperCase() + restoDaPalavra + " ";
			}
			String[] retirarUltimoEspaco = retorno.split(" ");
			String ultimaPalavra = retirarUltimoEspaco[retirarUltimoEspaco.length - 1];
			retorno = "";
			for (String string : retirarUltimoEspaco) {
				retorno += string;
				if (string.equals(ultimaPalavra)) {
					string.trim();
				} else {
					retorno += " ";
				}
			}
			return retorno;
		} catch (Exception e) {
			e.printStackTrace();
			return nome;
		}
	}
	
	public static String getMD5String(String value) throws NoSuchAlgorithmException {
		MessageDigest m = MessageDigest.getInstance("MD5");
		m.update(value.getBytes(),0,value.length());
		return (new BigInteger(1,m.digest()).toString(16));
		}

	
	
	public static void main(String[] args) {
		try {
			System.out.println(Util.getMD5String("ifba.05237673660"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	public static Usuario createUserFake() {
		Usuario user = new Usuario(1, "Monique Dantas", true, true);
		return user;
	}
	
	public static String base64Encode(String stringToEncode) {
		byte[] stringToEncodeBytes = stringToEncode.getBytes();
		return Base64.encodeBase64String(stringToEncodeBytes);
	}

	public static String base64Decode(String stringToDecode) {
		byte[] decodedBytes = Base64.decodeBase64(stringToDecode);
		return new String(decodedBytes);
	}
}
