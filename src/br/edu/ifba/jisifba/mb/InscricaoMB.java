package br.edu.ifba.jisifba.mb;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Inscricao;
import br.edu.ifba.jisifba.persist.entities.Modalidade;
import br.edu.ifba.jisifba.persist.entities.Perfil;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.entities.Status;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.service.EquipeService;
import br.edu.ifba.jisifba.service.InscricaoService;
import br.edu.ifba.jisifba.service.ModalidadeService;
import br.edu.ifba.jisifba.service.ServidorService;
import br.edu.ifba.jisifba.service.UsuarioService;
import br.edu.ifba.jisifba.util.JsfUtil;

@ManagedBean(name="inscricaoMB")
@ViewScoped
public class InscricaoMB implements Serializable {


	private static final long serialVersionUID = 4873044159337411978L;

	private List<Inscricao> lista;
	
	private List<Inscricao> listaPorCampus;
	
	private List<Inscricao> listaEquipe;
	
	private List<Inscricao> listaHomologadasPorCampus;
	
	private List<Inscricao> listaFilter;

	private List<Servidor> listaServidoresSolicitantes;
	
	private List<Servidor> listaServidoresInscritos;
	
	private List<Modalidade> modalidadesAtivas;
	
	private Inscricao inscricao;
	
	private Inscricao inscricaoFilter;
	
	private Usuario usuarioLogado;
	
	private Servidor servidor;
	
	private boolean podeInscrever;
	
	private Campus campusPesquisa;
	
	private boolean isServ;
	private boolean isDiretor;
	private boolean isComissaoCentral;
	private boolean isComissaoLocal;
	private boolean isChefeDelegacao;
	
    private Modalidade modalidadeEscolhida;
    
	@Inject
	private InscricaoService service;
	@Inject
	private UsuarioService usuarioService;
	@Inject
	private ModalidadeService modalidadeService;
	@Inject
	private ServidorService servidorService;
	@Inject
	private EquipeService equipeService;
	
	@Resource(lookup = "mail/criaUsuarioMailSession")
	private Session mailSession;
	
	@PostConstruct
	public void init()  {
		inscricao = new Inscricao();
		inscricaoFilter = new Inscricao();
		inscricaoFilter.getServidor().setHospedagem(null);
		campusPesquisa = new Campus();
		inscricao.setTitular(true);
		if (usuarioLogado == null) {
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			usuarioLogado = (Usuario) request.getSession().getAttribute("usuarioLogado");
			usuarioLogado = usuarioService.findById(usuarioLogado);
		}
		podeInscrever = false;
		if (usuarioLogado.getServidorList() != null && !usuarioLogado.getServidorList().isEmpty()) {
			servidor = usuarioLogado.getServidorList().get(0);
			podeInscrever = service.validate(servidor);
		}
		setarPermissoes();
		carregarLista();
	}
	
	public void carregarLista() {
		lista = new ArrayList<Inscricao>();
		listaFilter = new ArrayList<Inscricao>();
		listaEquipe = new ArrayList<Inscricao>();
		if (isServ && podeInscrever) {
			lista = service.findByServidor(servidor);
		} 
		if ((isDiretor) && podeInscrever) {
			lista = service.findByCampusStatus(servidor.getSetorId().getCampusId(), new Status(Status.ENVIADO_CHEFIA));
		}
		if ((isComissaoLocal || isChefeDelegacao) && podeInscrever) {
			lista = service.findByCampusStatus(servidor.getSetorId().getCampusId(), new Status(Status.AUTORIZADO_CHEFIA));
		}
		if ((usuarioLogado.getAdmin() || isComissaoCentral)) {
			lista = service.findAll();
		}
		if (servidor != null && servidor.getSetorId() != null) {
			for (Inscricao i : service.findAll()) {
				if (i.getServidor().getId().equals(servidor.getId()) && !lista.contains(i)) {
					lista.add(i);
				}
			}
		}
		modalidadesAtivas = new ArrayList<Modalidade>();
		if (podeInscrever) {
			modalidadesAtivas = modalidadeService.findAtivas(servidor);
		}
		if (servidor != null && servidor.getSetorId() != null) {
			listaPorCampus = service.findByCampusStatuSolicitacoes(servidor.getSetorId().getCampusId());
			listaHomologadasPorCampus = service.findByCampusStatus(servidor.getSetorId().getCampusId(), new Status(Status.HOMOLOGADO));
			
			listaServidoresSolicitantes = servidorService.findByCampusSolicitante(servidor.getSetorId().getCampusId());
			listaServidoresInscritos = servidorService.findByCampusHomologados(servidor.getSetorId().getCampusId());
			
		}
	}
	
	public void carregarListaEquipe(Inscricao inscricao) {
		
		listaEquipe = equipeService.findById(inscricao.getEquipeId()).getInscricaoList();
	}
	
	public String filtroSolicitacoes(int filtro) {
		if (filtro == 1) {
			lista = service.findByServidor(servidor);
		}
		if (filtro == 2) {
			if ((isDiretor)) {
				lista = service.findByCampusStatus(servidor.getSetorId().getCampusId(), new Status(Status.ENVIADO_CHEFIA));
			}
			if ((isComissaoLocal || isChefeDelegacao)) {
				lista = service.findByCampusStatus(servidor.getSetorId().getCampusId(), new Status(Status.AUTORIZADO_CHEFIA));
			}
		} 
		if (filtro == 3) {
			carregarLista();
		}
		return "/pages/inscricao.xhtml";
	}
	
	public void pesquisar() {
		inscricaoFilter.getServidor().getSetorId().setCampusId(campusPesquisa);
		listaFilter = service.findByFilter(inscricaoFilter);
	}
	
	private void setarPermissoes() {
		isServ = true;
		isChefeDelegacao = false;
		isComissaoCentral = false;
		isComissaoLocal = false;
		isDiretor = false;
		if (usuarioLogado.getPerfilId().getId().equals(Perfil.SERVIDOR)) {
			isServ = true;
		} 
		if (usuarioLogado.getPerfilId().getId().equals(Perfil.DIRETOR)) {
			isDiretor = true;
		} 
		if (usuarioLogado.getPerfilId().getId().equals(Perfil.COMISSAO_LOCAL)) {
			isComissaoLocal = true;
		}
		if (usuarioLogado.getPerfilId().getId().equals(Perfil.CHEFE_DELEGACAO)) {
			isComissaoLocal = true;
		} 
		if (usuarioLogado.getPerfilId().getId().equals(Perfil.COMISSAO_CENTRAL)) {
			isComissaoCentral = true;
		} 
	}
	
	public void prepararEditar(Inscricao inscricao) {
		this.inscricao = inscricao;
	}
	
	public void verificarVagas(Inscricao i) {
		i.setModalidade(modalidadeService.findSetandoAsVagas(i.getServidor().getSetorId().getCampusId(), i.getModalidade()));
		this.inscricao = i;
	}
	
	public void cadastrar() {
		usuarioLogado = usuarioService.findById(usuarioLogado);
		try {
			Inscricao result = service.insert(inscricao, usuarioLogado);
			if (result == null) {
				JsfUtil.addErrorMessage("Inscri��es para modalidade " + inscricao.getModalidade().getTipoId().getNome() + " n�o permitidos. Limite alcan�ado.");
			} else {
				JsfUtil.addSuccessMessage("Cadastro realizado com sucesso! Clique no bot�o ENVIAR para iniciar o processo de homologa��o de sua inscri��o. Verifique se n�o h� choque de hor�rio acessando o CALEND�RIO DE PROVAS.");
				try {
					if (usuarioLogado.getLdap()) {
						service.enviarEmailSolicitacao(servidor.getUserId().getUsername()+"@ifba.edu.br", servidor.getNome(), mailSession);
					} else {
						service.enviarEmailSolicitacao(servidor.getEmailPessoal(), servidor.getNome(), mailSession);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				carregarLista();
				JsfUtil.closeModal("cadastroinscricaodialog");
			}
		} catch (Exception e) {
			JsfUtil.addErrorMessage("J� existe inscri��o para esta modalidade.");
		}
	}
	
	public void atualizarStatus(Inscricao i, int statusNovo) {
		Status statusAntes = i.getStatusId();
		i.setStatusId(new Status(statusNovo));
		if (i.getStatusId().getId().equals(Status.HOMOLOGADO)) {
			String msg = service.validarHomologacao(i, servidor);
			if (msg != null) {
				JsfUtil.addErrorMessage(msg);
				i.setStatusId(statusAntes);
				return;
			}
		}
		if (i.getStatusId().getId().equals(Status.AUTORIZADO_CHEFIA) || i.getStatusId().getId().equals(Status.NAO_AUTORIZADO_CHEFIA)) {
			List<Inscricao> inscDoServidor = service.findByServidor(i.getServidor());
			for (Inscricao insc : inscDoServidor) {
				if (insc.getStatusId().getId().equals(statusAntes.getId())) {
					insc.setStatusId(i.getStatusId());
					service.update(insc, usuarioLogado);
				}
			}
		}
		Inscricao result = service.update(i, usuarioLogado);
		if (result == null) {
			JsfUtil.addErrorMessage("Inscri��es para modalidade " + i.getModalidade().getTipoId().getNome() + " n�o permitidos. Limite alcan�ado.");
		} else {
			JsfUtil.addSuccessMessage("Status atualizado com sucesso.");
			if (result.getStatusId().getId().equals(Status.HOMOLOGADO) || result.getStatusId().getId().equals(Status.INDEFERIDO) 
					|| result.getStatusId().getId().equals(Status.AUTORIZADO_CHEFIA) || result.getStatusId().getId().equals(Status.NAO_AUTORIZADO_CHEFIA)) {
				try {
					if (i.getServidor().getUserId().getLdap()) {
						service.enviarEmailMudancaStatus(i.getServidor().getUserId().getUsername()+"@ifba.edu.br", i.getServidor().getNome(), mailSession);
					} else {
						service.enviarEmailMudancaStatus(i.getServidor().getEmailPessoal(), i.getServidor().getNome(), mailSession);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		carregarLista();
		podeInscrever = service.validate(servidor);
	}
	
	public void editar() {
		service.update(inscricao, usuarioLogado);
		JsfUtil.addSuccessMessage("Atualiza��o realizado com sucesso!");
		carregarLista();
		JsfUtil.closeModal("cadastroinscricaodialog");
	}
	
	public void limpar() {
		inscricao = new Inscricao();
		inscricao.setTitular(true);
		modalidadesAtivas = modalidadeService.findAtivas(servidor);
	}
	
	
	public void postProcessXLS(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow header = sheet.getRow(0);
        for (int i = 0; i < 50; i++) {
        	 sheet.autoSizeColumn(i);
        }
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        for(int i=0; i < header.getPhysicalNumberOfCells();i++) {
            HSSFCell cell = header.getCell(i);
            cell.setCellStyle(cellStyle);
        }
    }
	
	public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = (Document) document;
        pdf.addTitle("Jogos dos Servidores IFBA 2017");
        pdf.setMargins(2f, 2f, 2f, 2f);  
        pdf.setPageSize(PageSize.A4.rotate());
        pdf.open();
//        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        /*String logo = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "imgs" + File.separator + "logo_ifba.jpg";
        Image img = Image.getInstance(logo);*/
		/*float scaler = ((pdf.getPageSize().getWidth() - pdf.leftMargin()
				- pdf.rightMargin() - 770) / img.getWidth()) * 100;*/
//		float scaler = 30f;
//		img.scalePercent(scaler);
        
//        pdf.add(img);
    }

	public List<Inscricao> getLista() {
		return lista;
	}

	public void setLista(List<Inscricao> lista) {
		this.lista = lista;
	}

	public Inscricao getInscricao() {
		return inscricao;
	}

	public void setInscricao(Inscricao inscricao) {
		this.inscricao = inscricao;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public List<Modalidade> getModalidadesAtivas() {
		return modalidadesAtivas;
	}

	public void setModalidadesAtivas(List<Modalidade> modalidadesAtivas) {
		this.modalidadesAtivas = modalidadesAtivas;
	}

	public boolean isPodeInscrever() {
		return podeInscrever;
	}

	public void setPodeInscrever(boolean podeInscrever) {
		this.podeInscrever = podeInscrever;
	}

	public boolean isServ() {
		return isServ;
	}

	public void setServ(boolean isServidor) {
		this.isServ = isServidor;
	}

	public boolean isDiretor() {
		return isDiretor;
	}

	public void setDiretor(boolean isDiretor) {
		this.isDiretor = isDiretor;
	}

	public boolean isComissaoCentral() {
		return isComissaoCentral;
	}

	public void setComissaoCentral(boolean isComissaoCentral) {
		this.isComissaoCentral = isComissaoCentral;
	}

	public boolean isComissaoLocal() {
		return isComissaoLocal;
	}

	public void setComissaoLocal(boolean isComissaoLocal) {
		this.isComissaoLocal = isComissaoLocal;
	}

	public boolean isChefeDelegacao() {
		return isChefeDelegacao;
	}

	public void setChefeDelegacao(boolean isChefeDelegacao) {
		this.isChefeDelegacao = isChefeDelegacao;
	}

	public List<Inscricao> getListaPorCampus() {
		return listaPorCampus;
	}

	public void setListaPorCampus(List<Inscricao> listaPorCampus) {
		this.listaPorCampus = listaPorCampus;
	}

	public List<Inscricao> getListaHomologadasPorCampus() {
		return listaHomologadasPorCampus;
	}

	public void setListaHomologadasPorCampus(List<Inscricao> listaHomologadasPorCampus) {
		this.listaHomologadasPorCampus = listaHomologadasPorCampus;
	}

	public Modalidade getModalidadeEscolhida() {
		return modalidadeEscolhida;
	}

	public void setModalidadeEscolhida(Modalidade modalidadeEscolhida) {
		this.modalidadeEscolhida = modalidadeEscolhida;
	}

	public Inscricao getInscricaoFilter() {
		return inscricaoFilter;
	}

	public void setInscricaoFilter(Inscricao inscricaoFilter) {
		this.inscricaoFilter = inscricaoFilter;
	}

	public List<Inscricao> getListaFilter() {
		return listaFilter;
	}

	public void setListaFilter(List<Inscricao> listaFilter) {
		this.listaFilter = listaFilter;
	}

	public List<Inscricao> getListaEquipe() {
		return listaEquipe;
	}

	public void setListaEquipe(List<Inscricao> listaEquipe) {
		this.listaEquipe = listaEquipe;
	}

	public List<Servidor> getListaServidoresSolicitantes() {
		return listaServidoresSolicitantes;
	}

	public void setListaServidoresSolicitantes(List<Servidor> listaServidoresSolicitantes) {
		this.listaServidoresSolicitantes = listaServidoresSolicitantes;
	}

	public List<Servidor> getListaServidoresInscritos() {
		return listaServidoresInscritos;
	}

	public void setListaServidoresInscritos(List<Servidor> listaServidoresInscritos) {
		this.listaServidoresInscritos = listaServidoresInscritos;
	}

	public Campus getCampusPesquisa() {
		return campusPesquisa;
	}

	public void setCampusPesquisa(Campus campusPesquisa) {
		this.campusPesquisa = campusPesquisa;
	}

	
}
