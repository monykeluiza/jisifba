package br.edu.ifba.jisifba.mb;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Equipe;
import br.edu.ifba.jisifba.persist.entities.Inscricao;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.entities.Setor;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.service.CampusService;
import br.edu.ifba.jisifba.service.EquipeService;
import br.edu.ifba.jisifba.service.InscricaoService;
import br.edu.ifba.jisifba.service.SetorService;

@ManagedBean(name="equipeMB")
@ViewScoped
public class EquipeMB implements Serializable {


	private static final long serialVersionUID = -7322455655295200368L;
	
	private List<Equipe> lista;

	private List<Campus> listaCampus;
	
	private List<Setor> listaSetor;
	
	private List<Inscricao> listaTimeAvulso;

	
	private Servidor servidor;
	
	private Usuario usuarioLogado;
	
	@Inject
	private EquipeService service;
	@Inject
	private CampusService campusService;
	@Inject
	private SetorService setorService;
	@Inject
	private InscricaoService inscricaoService;

	@PostConstruct
	public void init() {
		if (usuarioLogado == null) {
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			usuarioLogado = (Usuario) request.getSession().getAttribute("usuarioLogado");
		}
		carregarLista();
	}
	
	public void carregarLista() {
		lista = service.findAll();
		listaCampus = campusService.findAll();
		listaSetor = setorService.findAll();
		listaTimeAvulso = inscricaoService.findByTimeAvulso();
	}
	
	public List<Equipe> getLista() {
		return lista;
	}

	public void setLista(List<Equipe> lista) {
		this.lista = lista;
	}

	public List<Campus> getListaCampus() {
		return listaCampus;
	}

	public void setListaCampus(List<Campus> listaCampus) {
		this.listaCampus = listaCampus;
	}

	public List<Setor> getListaSetor() {
		return listaSetor;
	}

	public void setListaSetor(List<Setor> listaSetor) {
		this.listaSetor = listaSetor;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public List<Inscricao> getListaTimeAvulso() {
		return listaTimeAvulso;
	}

	public void setListaTimeAvulso(List<Inscricao> listaTimeAvulso) {
		this.listaTimeAvulso = listaTimeAvulso;
	}
	
}
