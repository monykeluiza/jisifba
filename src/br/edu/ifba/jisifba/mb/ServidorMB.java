package br.edu.ifba.jisifba.mb;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.util.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Docs;
import br.edu.ifba.jisifba.persist.entities.Perfil;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.entities.Setor;
import br.edu.ifba.jisifba.persist.entities.TipoDoc;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.service.CampusService;
import br.edu.ifba.jisifba.service.CrachaService;
import br.edu.ifba.jisifba.service.ServidorService;
import br.edu.ifba.jisifba.service.SetorService;
import br.edu.ifba.jisifba.service.UsuarioService;
import br.edu.ifba.jisifba.util.JsfUtil;

@ManagedBean(name="servidorMB")
@ViewScoped
public class ServidorMB implements Serializable {


	private static final long serialVersionUID = 8308633918826419833L;

	private List<Servidor> lista;
	
	private List<Campus> listaCampus;
	
	private List<Setor> listaSetor;
	
	private Servidor servidor;
	
	private Usuario usuarioLogado;
	
	private boolean renderDados;
	
	private boolean confirmacaoDados;
	
	private Docs documento;
	
	private Docs foto;
	
	private Docs atestado;
	
	private Docs termo;
	
	private boolean visualizar;
	
	private List<Docs> listaDocumentos;
	private StreamedContent dowloadFile;

	@Inject
	private ServidorService service;
	@Inject
	private CampusService campusService;
	@Inject
	private SetorService setorService;
	@Inject
	private UsuarioService usuarioService;
	@Inject
	private CrachaService crachaService;
	
	@PostConstruct
	public void init() {
		visualizar = false;
		if (usuarioLogado == null) {
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			usuarioLogado = (Usuario) request.getSession().getAttribute("usuarioLogado");
		}
		carregarLista();
		servidor = new Servidor();
		renderDados = false;
		listaDocumentos = new ArrayList<Docs>();
		verificarServidorVinculado();
		confirmacaoDados = false;
	}
	
	private void verificarServidorVinculado() {
		usuarioLogado = usuarioService.findById(usuarioLogado);
		if (usuarioLogado.getServidorList() == null || usuarioLogado.getServidorList().isEmpty()) {
			renderDados = false;
		} else {
			renderDados = true;
			if (servidor == null || servidor.getId() == null) {
				servidor = usuarioLogado.getServidorList().get(0);
			}
			listaDocumentos = servidor.getDocsList();
		}
	}
	public void carregarLista() {
		lista = service.findAll();
		listaCampus = campusService.findAll();
		listaSetor = setorService.findAll();
	}
	
	public void prepararEditar(Servidor serv) {
		servidor = serv;
		listaDocumentos = servidor.getDocsList();
		visualizar = true;
	}
	
	public void cadastrar() {
		servidor.setUserId(usuarioLogado);
		if (servidor.getId() == null) {
			Servidor result = service.insert(servidor, usuarioLogado);
			if (result == null) {
				JsfUtil.addErrorMessage("Cadastro n�o atualizado! Anexe a documenta��o exigida!");
			} else {
				JsfUtil.addSuccessMessage("Cadastro realizado com sucesso!");
				JsfUtil.closeModal("cadastroservidordialog");
			}
		} else { 
			Servidor result = service.update(servidor, usuarioLogado);
			if (result == null) {
				JsfUtil.addErrorMessage("Cadastro n�o atualizado! Anexe a documenta��o exigida!");
			} else {
				JsfUtil.addSuccessMessage("Cadastro realizado com sucesso!");
				JsfUtil.closeModal("cadastroservidordialog");
			}
		}
		carregarLista();
	}
	
	public void pesquisar() {
		servidor = service.findBySiapeCpf(servidor);
		if (servidor == null) {
			JsfUtil.addErrorMessage("Dados n�o encontrados!");
			servidor = new Servidor();
			renderDados = false;
		} else {
			if (servidor.getUserId() != null && servidor.getUserId().getId() != null) {
				JsfUtil.addErrorMessage("Servidor j� vinculado. Verifique suas informa��es e tente novamente ou entre em contato com a administra��o dos jogos.");
				servidor = new Servidor();
				renderDados = false;
			} else {
				renderDados = true;
			}
		}
	}
	
	public void uploadFileDocumento(FileUploadEvent event) {
		 try {
	        	documento = new Docs();
	        	documento.setDocumento(IOUtils.toByteArray(event.getFile().getInputstream()));
	        	documento.setNomeArquivo(event.getFile().getFileName());
	        	documento.setTipoDocId(new TipoDoc(TipoDoc.DOCUMENTO));
	        	documento.setData(new Date());
	        	documento.setServidorId(servidor);
				if (!documentoExiste(documento)) {
					listaDocumentos.add(documento);
					JsfUtil.addSuccessMessage("Arquivo "+ event.getFile().getFileName() + " transferido com sucesso!");
					servidor.setDocsList(listaDocumentos);
				} else {
					JsfUtil.addErrorMessage("Documento j� anexado. Exclua o documento e tente novamente.");
				}
			} catch (IOException e) {
				e.printStackTrace();
				JsfUtil.addErrorMessage("Ocorreu um erro ao transferir o arquivo! Por favor, tente novamente! ");
			}
	}
	
	public void removerDoc(Docs doc) {
		listaDocumentos.remove(doc);
		JsfUtil.addSuccessMessage("Arquivo removido! Para confirmar a exclus�o, salve suas informa��es.");
	}
	
	public void uploadFileFoto(FileUploadEvent event) {
		 try {
	        	documento = new Docs();
	        	documento.setDocumento(IOUtils.toByteArray(event.getFile().getInputstream()));
	        	documento.setNomeArquivo(event.getFile().getFileName());
	        	documento.setTipoDocId(new TipoDoc(TipoDoc.FOTO));
	        	documento.setData(new Date());
	        	documento.setServidorId(servidor);
				if (!documentoExiste(documento)) {
					listaDocumentos.add(documento);
					JsfUtil.addSuccessMessage("Arquivo "+ event.getFile().getFileName() + " transferido com sucesso!");
					servidor.setDocsList(listaDocumentos);
				} else {
					JsfUtil.addErrorMessage("Documento j� anexado. Exclua o documento e tente novamente.");
				}
			} catch (IOException e) {
				e.printStackTrace();
				JsfUtil.addErrorMessage("Ocorreu um erro ao transferir o arquivo! Por favor, tente novamente! ");
			}
	}
	
	public void uploadFileAtestado(FileUploadEvent event) {
        try {
        	documento = new Docs();
        	documento.setDocumento(IOUtils.toByteArray(event.getFile().getInputstream()));
        	documento.setNomeArquivo(event.getFile().getFileName());
        	documento.setTipoDocId(new TipoDoc(TipoDoc.ATESTADO));
        	documento.setData(new Date());
        	documento.setServidorId(servidor);
			if (!documentoExiste(documento)) {
				listaDocumentos.add(documento);
				JsfUtil.addSuccessMessage("Arquivo "+ event.getFile().getFileName() + " transferido com sucesso!");
				servidor.setDocsList(listaDocumentos);
			} else {
				JsfUtil.addErrorMessage("Documento j� anexado. Exclua o documento e tente novamente.");
			}
		} catch (IOException e) {
			e.printStackTrace();
			JsfUtil.addErrorMessage("Ocorreu um erro ao transferir o arquivo! Por favor, tente novamente! ");
		}
	}
	
	public void uploadFileTermo(FileUploadEvent event) {
        try {
        	documento = new Docs();
        	documento.setDocumento(IOUtils.toByteArray(event.getFile().getInputstream()));
        	documento.setNomeArquivo(event.getFile().getFileName());
        	documento.setTipoDocId(new TipoDoc(TipoDoc.TERMO));
        	documento.setData(new Date());
        	documento.setServidorId(servidor);
			if (!documentoExiste(documento)) {
				listaDocumentos.add(documento);
				JsfUtil.addSuccessMessage("Arquivo "+ event.getFile().getFileName() + " transferido com sucesso!");
				servidor.setDocsList(listaDocumentos);
			} else {
				JsfUtil.addErrorMessage("Documento j� anexado. Exclua o documento e tente novamente.");
			}
		} catch (IOException e) {
			e.printStackTrace();
			JsfUtil.addErrorMessage("Ocorreu um erro ao transferir o arquivo! Por favor, tente novamente! ");
		}
	}
	
	private boolean documentoExiste(Docs doc) {
		for (Docs d : listaDocumentos) {
			if (d.getTipoDocId().getId().equals(doc.getTipoDocId().getId())) {
				return true;
			}
		}
		return false;
	}
	
	public void download(Docs docs) {
		dowloadFile = JsfUtil.convertFichier(docs.getDocumento(), docs.getNomeArquivo());
	}
	
	public void downloadTermo() {
		 InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/pages/regulamento/termo_responsabilidade_jisifba2017.pdf");
		 dowloadFile = new DefaultStreamedContent(stream, "application/pdf","termo_responsabilidade_jisifba2017.pdf");
	}
	
	public void limpar() {
		servidor = new Servidor();
		renderDados = false;
	}
	
	public void gerarCracha() {
		crachaService.gerarCrachaServidorAtleta(service.findHomologados());
		List<Servidor> delegacao  = new ArrayList<Servidor>();
		for (Servidor serv : service.findAll()) {
			if (serv.getUserId() != null && serv.getUserId().getPerfilId() != null && (serv.getUserId().getPerfilId().getId().equals(Perfil.CHEFE_DELEGACAO) || serv.getUserId().getPerfilId().getId().equals(Perfil.COMISSAO_LOCAL))) {
				delegacao.add(serv);
			}
		}
		
		List<Servidor> comissao  = new ArrayList<Servidor>();
		for (Servidor serv : service.findAll()) {
			if (serv.getUserId() != null && serv.getUserId().getPerfilId() != null && (serv.getUserId().getPerfilId().getId().equals(Perfil.COMISSAO_CENTRAL))) {
				comissao.add(serv);
			}
		}
		
		crachaService.gerarCrachaChefeDelegacao(delegacao);
		crachaService.gerarCrachaServidorOrganizador(comissao);
		JsfUtil.addSuccessMessage("Crach� gerado com sucesso");
	}

	public List<Servidor> getLista() {
		return lista;
	}

	public void setLista(List<Servidor> lista) {
		this.lista = lista;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public List<Campus> getListaCampus() {
		return listaCampus;
	}

	public void setListaCampus(List<Campus> listaCampus) {
		this.listaCampus = listaCampus;
	}

	public List<Setor> getListaSetor() {
		return listaSetor;
	}

	public void setListaSetor(List<Setor> listaSetor) {
		this.listaSetor = listaSetor;
	}

	public boolean isRenderDados() {
		return renderDados;
	}

	public void setRenderDados(boolean renderDados) {
		this.renderDados = renderDados;
	}

	public boolean isConfirmacaoDados() {
		return confirmacaoDados;
	}

	public void setConfirmacaoDados(boolean confirmacaoDados) {
		this.confirmacaoDados = confirmacaoDados;
	}

	public Docs getDocumento() {
		return documento;
	}

	public void setDocumento(Docs documento) {
		this.documento = documento;
	}

	public Docs getFoto() {
		return foto;
	}

	public void setFoto(Docs foto) {
		this.foto = foto;
	}

	public Docs getAtestado() {
		return atestado;
	}

	public void setAtestado(Docs atestado) {
		this.atestado = atestado;
	}

	public List<Docs> getListaDocumentos() {
		return listaDocumentos;
	}

	public void setListaDocumentos(List<Docs> listaDocumentos) {
		this.listaDocumentos = listaDocumentos;
	}

	public StreamedContent getDowloadFile() {
		return dowloadFile;
	}

	public void setDowloadFile(StreamedContent dowloadFile) {
		this.dowloadFile = dowloadFile;
	}

	public boolean isVisualizar() {
		return visualizar;
	}

	public void setVisualizar(boolean visualizar) {
		this.visualizar = visualizar;
	}

	public Docs getTermo() {
		return termo;
	}

	public void setTermo(Docs termo) {
		this.termo = termo;
	}

}
