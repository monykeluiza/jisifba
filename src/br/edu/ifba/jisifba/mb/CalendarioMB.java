package br.edu.ifba.jisifba.mb;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import br.edu.ifba.jisifba.persist.entities.Usuario;

@ManagedBean(name="calendarioMB")
@ViewScoped
public class CalendarioMB implements Serializable {
	
	private static final long serialVersionUID = 7074895585253733239L;

	private Usuario usuarioLogado;
	
	private ScheduleModel eventModel;
	
	private ScheduleEvent event = new DefaultScheduleEvent();
	
	@PostConstruct
	public void init() {
		if (usuarioLogado == null) {
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			usuarioLogado = (Usuario) request.getSession().getAttribute("usuarioLogado");
		}
		eventModel = new DefaultScheduleModel();
		eventModel.addEvent(new DefaultScheduleEvent("INSCRI��ES", getDataInicioInscricoes(), getDataFimInscricoes(), true));
        eventModel.addEvent(new DefaultScheduleEvent("HOMOLOGA��ES", getDataInicioInscricoes(), getDataFimHomologacoes(), "event-1"));
        
        // DIA 26
        eventModel.addEvent(new DefaultScheduleEvent("CHEGADA DAS DELEGA��ES", getDataInicioDia26ByModalidade("delegacoes"),getDataFimDia26ByModalidade("delegacoes"), "event-1"));
        eventModel.addEvent(new DefaultScheduleEvent("CERIM�NIA DE ABERTURA", getDataInicioDia26ByModalidade("abertura"),getDataFimDia26ByModalidade("abertura"), "event-2"));
        
        // DIA 27
        eventModel.addEvent(new DefaultScheduleEvent("ATLETISMO", getDataInicioDia27ByModalidade("atletismo"),getDataFimDia27ByModalidade("atletismo"), "event-3"));
        eventModel.addEvent(new DefaultScheduleEvent("NATA��O", getDataInicioDia27ByModalidade("natacao"),getDataFimDia27ByModalidade("natacao"), "event-1"));
        eventModel.addEvent(new DefaultScheduleEvent("T�NIS DE MESA", getDataInicioDia27ByModalidade("tenis"),getDataFimDia27ByModalidade("tenis"), "event-2"));
        eventModel.addEvent(new DefaultScheduleEvent("Futsal - Grupo 1 - AxB", getDataInicioDia27ByModalidade("futsal1ab"),getDataFimDia27ByModalidade("futsal1ab"), "event-4"));
        eventModel.addEvent(new DefaultScheduleEvent("Futsal - Grupo 2 - AxB", getDataInicioDia27ByModalidade("futsal2ab"),getDataFimDia27ByModalidade("futsal2ab"), "event-5"));
        
        // DIA 28
        eventModel.addEvent(new DefaultScheduleEvent("ATLETISMO", getDataInicioDia28ByModalidade("ATLETISMO"),getDataFimDia28ByModalidade("ATLETISMO"), "event-1"));
        eventModel.addEvent(new DefaultScheduleEvent("V�LEI DE PRAIA 4X4", getDataInicioDia28ByModalidade("volei"),getDataFimDia28ByModalidade("volei"), "event-2"));
        eventModel.addEvent(new DefaultScheduleEvent("F.SOCIETY", getDataInicioDia28ByModalidade("society"),getDataFimDia28ByModalidade("society"), "event-3"));
        eventModel.addEvent(new DefaultScheduleEvent("FUTSAL", getDataInicioDia28ByModalidade("FUTSAL"),getDataFimDia28ByModalidade("FUTSAL"), "event-4"));
        eventModel.addEvent(new DefaultScheduleEvent("VOLEIBOL", getDataInicioDia28ByModalidade("VOLEIBOL"),getDataFimDia28ByModalidade("VOLEIBOL"), "event-5"));
        eventModel.addEvent(new DefaultScheduleEvent("JUST DANCE", getDataInicioDia28ByModalidade("DANCE"),getDataFimDia28ByModalidade("DANCE"), "event-6"));
        eventModel.addEvent(new DefaultScheduleEvent("FIFA", getDataInicioDia28ByModalidade("FIFA"),getDataFimDia28ByModalidade("FIFA"), "event-1"));
        eventModel.addEvent(new DefaultScheduleEvent("T�NIS DE MESA", getDataInicioDia28ByModalidade("TENIS"),getDataFimDia28ByModalidade("TENIS"), "event-2"));
        eventModel.addEvent(new DefaultScheduleEvent("XADREZ", getDataInicioDia28ByModalidade("XADREZ"),getDataFimDia28ByModalidade("XADREZ"), "event-3"));
        eventModel.addEvent(new DefaultScheduleEvent("F.SOCIETY", getDataInicioDia28ByModalidade("society"),getDataFimDia28ByModalidade("society"), "event-4"));
        eventModel.addEvent(new DefaultScheduleEvent("BASQUETE", getDataInicioDia28ByModalidade("BASQUETE"),getDataFimDia28ByModalidade("BASQUETE"), "event-3"));

        
        // DIA 29
        eventModel.addEvent(new DefaultScheduleEvent("BALEADO", getDataInicioDia29ByModalidade("BALEADO"),getDataFimDia29ByModalidade("BALEADO"), "event-1"));
        eventModel.addEvent(new DefaultScheduleEvent("F.SOCIETY", getDataInicioDia29ByModalidade("society"),getDataFimDia29ByModalidade("society"), "event-3"));
        eventModel.addEvent(new DefaultScheduleEvent("VOLEIBOL", getDataInicioDia29ByModalidade("VOLEIBOL"),getDataFimDia29ByModalidade("VOLEIBOL"), "event-5"));
        eventModel.addEvent(new DefaultScheduleEvent("XADREZ", getDataInicioDia29ByModalidade("XADREZ"),getDataFimDia29ByModalidade("XADREZ"), "event-2"));
        eventModel.addEvent(new DefaultScheduleEvent("BASQUETE", getDataInicioDia29ByModalidade("BASQUETE"),getDataFimDia29ByModalidade("BASQUETE"), "event-6"));
	}
	
	public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
    }
	
	private Calendar today() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);
        return calendar;
    }
	
	private Date getDataInicioInscricoes() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 10);
        t.set(Calendar.MONTH, 10-1);
        return t.getTime();
    }
	
	private Date getDataFimInscricoes() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 22+1);
        t.set(Calendar.MONTH, 10-1);
        return t.getTime();
    }
	
	private Date getDataFimHomologacoes() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 25+1);
        t.set(Calendar.MONTH, 10-1);
        return t.getTime();
    }
	
	private Date getDataInicioDia26ByModalidade(String modalidade) {
		Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 26);
        t.set(Calendar.MONTH, 11-1);
        if (modalidade.equalsIgnoreCase("delegacoes")) {
        	t.set(Calendar.AM_PM, Calendar.AM);
        	t.set(Calendar.HOUR_OF_DAY, 8);
        	t.set(Calendar.MINUTE, 30);
        }
        
        if (modalidade.equalsIgnoreCase("abertura")) {
        	t.set(Calendar.HOUR_OF_DAY, 19);
        	t.set(Calendar.MINUTE, 00);
        }
        
        return t.getTime();
	}
	
	private Date getDataFimDia26ByModalidade(String modalidade) {
		Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 26);
        t.set(Calendar.MONTH, 11-1);
        if (modalidade.equalsIgnoreCase("delegacoes")) {
        	t.set(Calendar.HOUR_OF_DAY, 18);
        	t.set(Calendar.MINUTE, 30);
        }
        
        if (modalidade.equalsIgnoreCase("abertura")) {
        	t.set(Calendar.HOUR_OF_DAY, 22);
        	t.set(Calendar.MINUTE, 00);
        }
        return t.getTime();
	}
	
	private Date getDataInicioDia27ByModalidade(String modalidade) {
		Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 27);
        t.set(Calendar.MONTH, 11-1);
        if (modalidade.equalsIgnoreCase("atletismo")) {
        	t.set(Calendar.HOUR_OF_DAY, 8);
        	t.set(Calendar.MINUTE, 30);
        }
        
        if (modalidade.equalsIgnoreCase("natacao")) {
        	t.set(Calendar.HOUR_OF_DAY, 14);
        	t.set(Calendar.MINUTE, 20);
        }
        
        if (modalidade.equalsIgnoreCase("tenis")) {
        	t.set(Calendar.HOUR_OF_DAY, 8);
        	t.set(Calendar.MINUTE, 30);
        }
        
        if (modalidade.equalsIgnoreCase("futsal1ab")) {
        	t.set(Calendar.HOUR_OF_DAY, 8);
        	t.set(Calendar.MINUTE, 30);
        }
        if (modalidade.equalsIgnoreCase("futsal2ab")) {
        	t.set(Calendar.HOUR_OF_DAY, 9);
        	t.set(Calendar.MINUTE, 20);
        }
        if (modalidade.equalsIgnoreCase("futsal3ab")) {
        	t.set(Calendar.HOUR_OF_DAY, 10);
        	t.set(Calendar.MINUTE, 10);
        }
        if (modalidade.equalsIgnoreCase("futsal4ab")) {
        	t.set(Calendar.HOUR_OF_DAY, 11);
        	t.set(Calendar.MINUTE, 00);
        }
        
        if (modalidade.equalsIgnoreCase("futsal1ac")) {
        	t.set(Calendar.HOUR_OF_DAY, 13);
        	t.set(Calendar.MINUTE, 30);
        }
        if (modalidade.equalsIgnoreCase("futsal2ac")) {
        	t.set(Calendar.HOUR_OF_DAY, 14);
        	t.set(Calendar.MINUTE, 20);
        }
        if (modalidade.equalsIgnoreCase("futsal3ac")) {
        	t.set(Calendar.HOUR_OF_DAY, 14);
        	t.set(Calendar.MINUTE, 20);
        }
        if (modalidade.equalsIgnoreCase("futsal4ac")) {
        	t.set(Calendar.HOUR_OF_DAY, 15);
        	t.set(Calendar.MINUTE, 10);
        }
        if (modalidade.equalsIgnoreCase("voleibol")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 00);
        }
        if (modalidade.equalsIgnoreCase("societyab")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 50);
        }
        if (modalidade.equalsIgnoreCase("societycd")) {
        	t.set(Calendar.HOUR_OF_DAY, 17);
        	t.set(Calendar.MINUTE, 40);
        }
        
        if (modalidade.equalsIgnoreCase("baleado")) {
        	t.set(Calendar.HOUR_OF_DAY, 18);
        	t.set(Calendar.MINUTE, 30);
        }
        
        if (modalidade.equalsIgnoreCase("basquete")) {
        	t.set(Calendar.HOUR_OF_DAY, 14);
        	t.set(Calendar.MINUTE, 20);
        }
        if (modalidade.equalsIgnoreCase("volei")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 50);
        }
        
        
        
        
       
        return t.getTime();
	}
	
	private Date getDataFimDia27ByModalidade(String modalidade) {
		Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 27);
        t.set(Calendar.MONTH, 11-1);
        
        if (modalidade.equalsIgnoreCase("atletismo")) {
        	t.set(Calendar.HOUR_OF_DAY, 14);
        	t.set(Calendar.MINUTE, 20);
        }
        
        if (modalidade.equalsIgnoreCase("natacao")) {
        	t.set(Calendar.HOUR_OF_DAY, 18);
        	t.set(Calendar.MINUTE, 30);
        }
        
        if (modalidade.equalsIgnoreCase("tenis")) {
        	t.set(Calendar.HOUR_OF_DAY, 18);
        	t.set(Calendar.MINUTE, 30);
        }
        
        if (modalidade.equalsIgnoreCase("futsal1ab")) {
        	t.set(Calendar.HOUR_OF_DAY, 9);
        	t.set(Calendar.MINUTE, 20);
        }
        if (modalidade.equalsIgnoreCase("futsal2ab")) {
        	t.set(Calendar.HOUR_OF_DAY, 10);
        	t.set(Calendar.MINUTE, 10);
        }
        if (modalidade.equalsIgnoreCase("futsal3ab")) {
        	t.set(Calendar.HOUR_OF_DAY, 10);
        	t.set(Calendar.MINUTE, 00);
        }
        if (modalidade.equalsIgnoreCase("futsal4ab")) {
        	t.set(Calendar.HOUR_OF_DAY, 11);
        	t.set(Calendar.MINUTE, 50);
        }
        
        if (modalidade.equalsIgnoreCase("futsal1ac")) {
        	t.set(Calendar.HOUR_OF_DAY, 14);
        	t.set(Calendar.MINUTE, 20);
        }
        if (modalidade.equalsIgnoreCase("futsal2ac")) {
        	t.set(Calendar.HOUR_OF_DAY, 15);
        	t.set(Calendar.MINUTE, 10);
        }
        if (modalidade.equalsIgnoreCase("futsal3ac")) {
        	t.set(Calendar.HOUR_OF_DAY, 15);
        	t.set(Calendar.MINUTE, 10);
        }
        if (modalidade.equalsIgnoreCase("futsal4ac")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 00);
        }
        if (modalidade.equalsIgnoreCase("voleibol")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 50);
        }
        if (modalidade.equalsIgnoreCase("societyab")) {
        	t.set(Calendar.HOUR_OF_DAY, 17);
        	t.set(Calendar.MINUTE, 40);
        }
        if (modalidade.equalsIgnoreCase("societycd")) {
        	t.set(Calendar.HOUR_OF_DAY, 18);
        	t.set(Calendar.MINUTE, 30);
        }
        
        if (modalidade.equalsIgnoreCase("baleado")) {
        	t.set(Calendar.HOUR_OF_DAY, 19);
        	t.set(Calendar.MINUTE, 00);
        }
        
        if (modalidade.equalsIgnoreCase("basquete")) {
        	t.set(Calendar.HOUR_OF_DAY, 15);
        	t.set(Calendar.MINUTE, 10);
        }
        if (modalidade.equalsIgnoreCase("volei")) {
        	t.set(Calendar.HOUR_OF_DAY, 17);
        	t.set(Calendar.MINUTE, 40);
        }
        return t.getTime();
	}
	
	
	private Date getDataInicioDia28ByModalidade(String modalidade) {
		Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 28);
        t.set(Calendar.MONTH, 11-1);
        if (modalidade.equalsIgnoreCase("atletismo") || modalidade.equalsIgnoreCase("futsal")) {
        	t.set(Calendar.AM_PM, Calendar.AM);
        	t.set(Calendar.HOUR_OF_DAY, 8);
        	t.set(Calendar.MINUTE, 30);
        }
        
        if (modalidade.equalsIgnoreCase("volei")) {
        	t.set(Calendar.HOUR_OF_DAY, 11);
        	t.set(Calendar.MINUTE, 50);
        }
        
        if (modalidade.equalsIgnoreCase("dance") || modalidade.equalsIgnoreCase("fifa")  || modalidade.equalsIgnoreCase("tenis")  || modalidade.equalsIgnoreCase("xadrez")) {
        	t.set(Calendar.HOUR_OF_DAY, 14);
        	t.set(Calendar.MINUTE, 20);
        }
        if (modalidade.equalsIgnoreCase("voleibol")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 50);
        }
        if (modalidade.equalsIgnoreCase("SOCIETY")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 00);
        }
        if (modalidade.equalsIgnoreCase("basquete")) {
        	t.set(Calendar.HOUR_OF_DAY, 18);
        	t.set(Calendar.MINUTE, 30);
        }
        return t.getTime();
	}
	
	private Date getDataFimDia28ByModalidade(String modalidade) {
		Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 28);
        t.set(Calendar.MONTH, 11-1);
        if (modalidade.equalsIgnoreCase("atletismo")) {
        	t.set(Calendar.HOUR_OF_DAY, 14);
        	t.set(Calendar.MINUTE, 20);
        }
        
        if (modalidade.equalsIgnoreCase("futsal")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 00);
        }
        
        if (modalidade.equalsIgnoreCase("volei")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 00);
        }
        
        if (modalidade.equalsIgnoreCase("dance") || modalidade.equalsIgnoreCase("fifa")  || modalidade.equalsIgnoreCase("tenis")  || modalidade.equalsIgnoreCase("xadrez")) {
        	t.set(Calendar.HOUR_OF_DAY, 19);
        	t.set(Calendar.MINUTE, 00);
        }
        if (modalidade.equalsIgnoreCase("voleibol")) {
        	t.set(Calendar.HOUR_OF_DAY, 17);
        	t.set(Calendar.MINUTE, 40);
        }
        if (modalidade.equalsIgnoreCase("SOCIETY")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 50);
        }
        if (modalidade.equalsIgnoreCase("basquete")) {
        	t.set(Calendar.HOUR_OF_DAY, 19);
        	t.set(Calendar.MINUTE, 00);
        }
        return t.getTime();
	}
	
	private Date getDataInicioDia29ByModalidade(String modalidade) {
		Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 29);
        t.set(Calendar.MONTH, 11-1);
        if (modalidade.equalsIgnoreCase("baleado") || modalidade.equalsIgnoreCase("xadrez") || modalidade.equalsIgnoreCase("futsal")) {
        	t.set(Calendar.HOUR_OF_DAY, 8);
        	t.set(Calendar.MINUTE, 30);
        }
        
        if (modalidade.equalsIgnoreCase("basquete")) {
        	t.set(Calendar.HOUR_OF_DAY, 10);
        	t.set(Calendar.MINUTE, 10);
        }
        
        
        if (modalidade.equalsIgnoreCase("SOCIETY")) {
        	t.set(Calendar.HOUR_OF_DAY, 14);
        	t.set(Calendar.MINUTE, 20);
        }
        return t.getTime();
	}
	
	private Date getDataFimDia29ByModalidade(String modalidade) {
		Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_MONTH, 29);
        t.set(Calendar.MONTH, 11-1);
        if (modalidade.equalsIgnoreCase("baleado")) {
        	t.set(Calendar.HOUR_OF_DAY, 12);
        	t.set(Calendar.MINUTE, 40);
        }
        
        if (modalidade.equalsIgnoreCase("xadrez")) {
        	t.set(Calendar.HOUR_OF_DAY, 19);
        	t.set(Calendar.MINUTE, 00);
        }
        
        if (modalidade.equalsIgnoreCase("basquete")) {
        	t.set(Calendar.HOUR_OF_DAY, 11);
        	t.set(Calendar.MINUTE, 00);
        }
        
        if (modalidade.equalsIgnoreCase("futsal")) {
        	t.set(Calendar.HOUR_OF_DAY, 17);
        	t.set(Calendar.MINUTE, 40);
        }
        
        if (modalidade.equalsIgnoreCase("SOCIETY")) {
        	t.set(Calendar.HOUR_OF_DAY, 16);
        	t.set(Calendar.MINUTE, 50);
        }
        return t.getTime();
	}

	public ScheduleModel getEventModel() {
		
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}
	
}