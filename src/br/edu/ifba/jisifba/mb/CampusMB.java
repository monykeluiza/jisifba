package br.edu.ifba.jisifba.mb;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.service.CampusService;
import br.edu.ifba.jisifba.service.UsuarioService;

@ManagedBean(name="campusMB")
@ViewScoped
public class CampusMB implements Serializable {

	private static final long serialVersionUID = -5186592514267698547L;

	private List<Campus> lista;
	
	private Usuario usuarioLogado;
	
	@Inject
	private CampusService service;
	
	@Inject
	private UsuarioService usuarioService;
	
	@PostConstruct
	public void init() {
		if (usuarioLogado == null) {
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			usuarioLogado = (Usuario) request.getSession().getAttribute("usuarioLogado");
		}
		if (usuarioLogado != null) {
			usuarioLogado = usuarioService.findById(usuarioLogado);
		}
		carregarLista();
	}
	
	public void carregarLista() {
		lista = service.findSetandoVagas();
	}
	
	
	public void postProcessXLS(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow header = sheet.getRow(0);
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);
        sheet.autoSizeColumn(7);
        sheet.autoSizeColumn(8);
        sheet.autoSizeColumn(9);
        sheet.autoSizeColumn(10);
        sheet.autoSizeColumn(11);
        sheet.autoSizeColumn(12);
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        for(int i=0; i < header.getPhysicalNumberOfCells();i++) {
            HSSFCell cell = header.getCell(i);
            cell.setCellStyle(cellStyle);
        }
    }
	
	public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = (Document) document;
        pdf.addTitle("Jogos dos Servidores IFBA 2017");
        pdf.setMargins(5f, 5f, 5f, 5f);  
        pdf.setPageSize(PageSize.A4.rotate());
        pdf.open();
//        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        /*String logo = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "imgs" + File.separator + "logo_ifba.jpg";
        Image img = Image.getInstance(logo);*/
		/*float scaler = ((pdf.getPageSize().getWidth() - pdf.leftMargin()
				- pdf.rightMargin() - 770) / img.getWidth()) * 100;*/
//		float scaler = 30f;
//		img.scalePercent(scaler);
        
//        pdf.add(img);
    }
	

	public List<Campus> getLista() {
		return lista;
	}

	public void setLista(List<Campus> lista) {
		this.lista = lista;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

}
