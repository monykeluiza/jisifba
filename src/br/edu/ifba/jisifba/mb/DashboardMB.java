package br.edu.ifba.jisifba.mb;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.model.chart.PieChartModel;

import br.edu.ifba.jisifba.persist.entities.Inscricao;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.entities.Status;
import br.edu.ifba.jisifba.service.InscricaoService;
import br.edu.ifba.jisifba.service.ServidorService;

@ManagedBean(name="dashboardMB")
@ViewScoped
public class DashboardMB implements Serializable {


	private static final long serialVersionUID = -3342400651429961202L;
	
	private int totalServidoresSolicitantes;
	private int totalServidoresHomologados;
	private int totalSolicitacoes;
	private int totalInscricoesHomologadas;
	private int totalAProcessar;
	private int totalRestantes;
	
	@Inject
	private InscricaoService inscricaoService;
	
	@Inject 
	private ServidorService servidorService;

	private PieChartModel pieInscricoesSexo;
	private PieChartModel pieInscricoesTipo;
	private PieChartModel pieHospedagem;

	
	@PostConstruct
	public void init() {
		totalSolicitacoes = 0;
		for (Inscricao ins : inscricaoService.findAll()) {
			if (!ins.getStatusId().getId().equals(Status.CANCELADO) && !ins.getStatusId().getId().equals(Status.NOVO)) {
				totalSolicitacoes++;
			}
		}
		totalInscricoesHomologadas = inscricaoService.findByStatus(new Status(Status.HOMOLOGADO)).size();
		totalAProcessar = 0;
		for (Inscricao ins : inscricaoService.findAll()) {
			if (ins.getStatusId().getId().equals(Status.AUTORIZADO_CHEFIA) || ins.getStatusId().getId().equals(Status.ENVIADO_CHEFIA)) {
				totalAProcessar++;
			}
		}
		totalServidoresHomologados = servidorService.findHomologados().size();
		totalServidoresSolicitantes = servidorService.findSolicitantes().size();
		totalRestantes = 301 - totalServidoresHomologados;
	}
	 
    public PieChartModel getPieInscricoesSexo() {
    	int qtdFeminino = 0;
    	int qtdMasculino = 0;
    	
    	for (Servidor serv : servidorService.findHomologados()) {
			if (serv.getSexo().equals('M')) {
				qtdMasculino++;
			} else {
				qtdFeminino++;
			}
		}
    	
    	pieInscricoesSexo = new PieChartModel();
    	pieInscricoesSexo.getData().put("Masculino", qtdMasculino);
        pieInscricoesSexo.getData().put("Feminino", qtdFeminino);
         
        pieInscricoesSexo.setTitle("P�blico");
        pieInscricoesSexo.setLegendPosition("ne");
        pieInscricoesSexo.setShowDataLabels(true);
        pieInscricoesSexo.setFill(false);
        return pieInscricoesSexo;
    }
    
    public PieChartModel getPieInscricoesTipo() {
    	int qtdColetivo = 0;
    	int qtdInvididual = 0;
    	
    	for (Inscricao ins : inscricaoService.findByStatus(new Status(Status.HOMOLOGADO))) {
			if (ins.getModalidade().getTipoId().getId().equals(2)) {
				qtdColetivo++;
			} else {
				qtdInvididual++;
			}
		}
    	
    	pieInscricoesTipo = new PieChartModel();
    	pieInscricoesTipo.getData().put("Coletivo", qtdColetivo);
    	pieInscricoesTipo.getData().put("Individual", qtdInvididual);
         
    	pieInscricoesTipo.setTitle("Modalidade");
    	pieInscricoesTipo.setLegendPosition("ne");
    	pieInscricoesTipo.setShowDataLabels(true);
    	pieInscricoesTipo.setFill(false);
        return pieInscricoesTipo;
    }
    
    public PieChartModel getPieHospedagem() {
    	int qtdSim = 0;
    	int qtdNao = 0;
    	
    	for (Servidor serv : servidorService.findHomologados()) {
			if (serv != null && serv.getHospedagem()!=null && serv.getHospedagem()) {
				qtdSim++;
			} else {
				qtdNao++;
			}
		}
    	
    	pieHospedagem = new PieChartModel();
    	pieHospedagem.getData().put("Sim", qtdSim);
    	pieHospedagem.getData().put("N�o", qtdNao);
         
    	pieHospedagem.setTitle("Hospedagem");
    	pieHospedagem.setLegendPosition("ne");
    	pieHospedagem.setShowDataLabels(true);
    	pieHospedagem.setFill(false);
        return pieHospedagem;
	}

	public int getTotalSolicitacoes() {
		return totalSolicitacoes;
	}

	public void setTotalSolicitacoes(int totalSolicitacoes) {
		this.totalSolicitacoes = totalSolicitacoes;
	}

	public int getTotalInscricoesHomologadas() {
		return totalInscricoesHomologadas;
	}

	public void setTotalInscricoesHomologadas(int totalInscricoesHomologadas) {
		this.totalInscricoesHomologadas = totalInscricoesHomologadas;
	}

	public int getTotalAProcessar() {
		return totalAProcessar;
	}

	public void setTotalAProcessar(int totalAProcessar) {
		this.totalAProcessar = totalAProcessar;
	}

	public int getTotalRestantes() {
		return totalRestantes;
	}

	public void setTotalRestantes(int totalRestantes) {
		this.totalRestantes = totalRestantes;
	}

	public void setPieHospedagem(PieChartModel pieHospedagem) {
		this.pieHospedagem = pieHospedagem;
	}

	public int getTotalServidoresSolicitantes() {
		return totalServidoresSolicitantes;
	}

	public void setTotalServidoresSolicitantes(int totalServidoresSolicitantes) {
		this.totalServidoresSolicitantes = totalServidoresSolicitantes;
	}

	public int getTotalServidoresHomologados() {
		return totalServidoresHomologados;
	}

	public void setTotalServidoresHomologados(int totalServidoresHomologados) {
		this.totalServidoresHomologados = totalServidoresHomologados;
	}
}
