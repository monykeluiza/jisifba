package br.edu.ifba.jisifba.mb;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.service.AutenticacaoService;
import br.edu.ifba.jisifba.util.JsfUtil;


@ManagedBean(name="autenticacaoMb") 
@SessionScoped
public class AutenticacaoMB implements Serializable {

	private static final long serialVersionUID = -716833992079367241L;
	private Usuario usuario;
	@Inject
	private AutenticacaoService autenticacaoService;
	
	private boolean renderUserLogado = false;
	private boolean renderUserLogadoAdmin = false;
	private Date dataAtual = new Date();
	
	public String autenticar() {
		try {
			usuario = autenticacaoService.autenticar(usuario);
			renderUserLogadoAdmin = (usuario.getAdmin());
			return doLogin();
		} catch (Exception e) {
			renderUserLogadoAdmin = false;
			JsfUtil.addErrorMessage(e.getMessage());
			e.printStackTrace();
			return "";
		}
	}
	
	public String doLogin() { 
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		session.setAttribute("usuarioLogado", usuario);
		renderUserLogado = true;
		try {
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			FacesContext.getCurrentInstance().getExternalContext().redirect(request.getContextPath() + "/pages/welcome.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "/index.xhtml";
	}
	
	public String doLogout() { 
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		session.setAttribute("usuarioLogado", null);
		renderUserLogado = false;
		try {
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			FacesContext.getCurrentInstance().getExternalContext().redirect(request.getContextPath() + "/index.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "/index.xhtml";
	}
		
	public Usuario getUsuario() {
		if (usuario == null) {
			usuario = new Usuario();
		}
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isRenderUserLogado() {
		return renderUserLogado;
	}

	public void setRenderUserLogado(boolean renderUserLogado) {
		this.renderUserLogado = renderUserLogado;
	}

	public Date getDataAtual() {
		return dataAtual;
	}

	public void setDataAtual(Date dataAtual) {
		this.dataAtual = dataAtual;
	}

	public boolean isRenderUserLogadoAdmin() {
		return renderUserLogadoAdmin;
	}

	public void setRenderUserLogadoAdmin(boolean renderUserLogadoAdmin) {
		this.renderUserLogadoAdmin = renderUserLogadoAdmin;
	} 
	
	
	
	
	
	
	

}
