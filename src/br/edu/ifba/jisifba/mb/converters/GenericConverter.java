package br.edu.ifba.jisifba.mb.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Equipe;
import br.edu.ifba.jisifba.persist.entities.Modalidade;
import br.edu.ifba.jisifba.persist.entities.Setor;
import br.edu.ifba.jisifba.persist.entities.Tipo;

@FacesConverter("genericConverter")
public class GenericConverter implements Converter {
    
	@Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
        if (value != null && !value.isEmpty()) {
            return (Object) uiComponent.getAttributes().get(value);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value instanceof Campus) {
        	Campus entity= (Campus) value;
        	if (entity != null && entity.getId() != null) {
               uiComponent.getAttributes().put( entity.getId().toString(), entity);
        		 return entity.getId().toString();
            }
        }
        
        if (value instanceof Setor) {
        	Setor entity= (Setor) value;
        	if (entity != null && entity.getId() != null) {
               uiComponent.getAttributes().put( entity.getId().toString(), entity);
        		 return entity.getId().toString();
            }
        }
        
        if (value instanceof Modalidade) {
        	Modalidade entity= (Modalidade) value;
        	if (entity != null && entity.getId() != null) {
               uiComponent.getAttributes().put( entity.getId().toString(), entity);
        		 return entity.getId().toString();
            }
        }
        
        if (value instanceof Tipo) {
        	Tipo entity= (Tipo) value;
        	if (entity != null && entity.getId() != null) {
               uiComponent.getAttributes().put( entity.getId().toString(), entity);
        		 return entity.getId().toString();
            }
        }
        if (value instanceof Equipe) {
        	Equipe entity= (Equipe) value;
        	if (entity != null && entity.getId() != null) {
               uiComponent.getAttributes().put( entity.getId().toString(), entity);
        		 return entity.getId().toString();
            }
        }
        
        return "";
    }
}
