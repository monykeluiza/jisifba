package br.edu.ifba.jisifba.mb;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Modalidade;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.service.ModalidadeService;
import br.edu.ifba.jisifba.service.UsuarioService;
import br.edu.ifba.jisifba.util.JsfUtil;

@ManagedBean(name="modalidadeMB")
@ViewScoped
public class ModalidadeMB implements Serializable {

	private static final long serialVersionUID = -5186592514267698547L;

	private List<Modalidade> lista;
	
	private List<Modalidade> listaAdm;
	
	private Modalidade modalidade;
	
	private Usuario usuarioLogado;
	
	private Campus campusSelecionado;
	
	@Inject
	private ModalidadeService service;
	
	@Inject
	private UsuarioService usuarioService;
	
	@PostConstruct
	public void init() {
		if (usuarioLogado == null) {
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			usuarioLogado = (Usuario) request.getSession().getAttribute("usuarioLogado");
		}
		if (usuarioLogado != null) {
			usuarioLogado = usuarioService.findById(usuarioLogado);
		}
		carregarLista();
		modalidade = new Modalidade();
	}
	
	public void carregarLista() {
		if (usuarioLogado.getServidorList() != null && !usuarioLogado.getServidorList().isEmpty() && usuarioLogado.getServidorList().get(0).getSetorId() != null) {
			lista = service.findSetandoAsVagas(usuarioLogado.getServidorList().get(0).getSetorId().getCampusId());
		} else {
			lista = new ArrayList<Modalidade>();
		}
		listaAdm = service.findSetandoAsVagas();
	}
	
	public void prepararEditar(Modalidade modalidade) {
		this.modalidade = modalidade;
	}
	
	public void cadastrar() {
		if (modalidade.getId() == null) {
			service.insert(modalidade, usuarioLogado);
			JsfUtil.addSuccessMessage("Cadastro realizado com sucesso!");
		} else { 
			service.update(modalidade, usuarioLogado);
			JsfUtil.addSuccessMessage("Cadastro atualizado com sucesso!");
		}
		carregarLista();
		JsfUtil.closeModal("cadastrotipotreinodialog");
	}
	
	public void ativar(Modalidade modalidade) {
		modalidade.setAtiva(true);
		modalidade = service.update(modalidade, usuarioLogado);
		JsfUtil.addSuccessMessage("Ativa��o realizada com sucesso.");
	}
	
	public void desativar(Modalidade modalidade) {
		modalidade.setAtiva(false);
		modalidade = service.update(modalidade, usuarioLogado);
		JsfUtil.addSuccessMessage("Desativa��o realizada com sucesso.");
	}
	
	public void postProcessXLS(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow header = sheet.getRow(0);
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);
        sheet.autoSizeColumn(7);
        sheet.autoSizeColumn(8);
        sheet.autoSizeColumn(9);
        sheet.autoSizeColumn(10);
        sheet.autoSizeColumn(11);
        sheet.autoSizeColumn(12);
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        for(int i=0; i < header.getPhysicalNumberOfCells();i++) {
            HSSFCell cell = header.getCell(i);
            cell.setCellStyle(cellStyle);
        }
    }
	
	public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = (Document) document;
        pdf.addTitle("Jogos dos Servidores IFBA 2017");
        pdf.setMargins(5f, 5f, 5f, 5f);  
        pdf.setPageSize(PageSize.A4.rotate());
        pdf.open();
//        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        /*String logo = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "imgs" + File.separator + "logo_ifba.jpg";
        Image img = Image.getInstance(logo);*/
		/*float scaler = ((pdf.getPageSize().getWidth() - pdf.leftMargin()
				- pdf.rightMargin() - 770) / img.getWidth()) * 100;*/
//		float scaler = 30f;
//		img.scalePercent(scaler);
        
//        pdf.add(img);
    }
	
	public void limpar() {
		modalidade = new Modalidade();
	}

	public List<Modalidade> getLista() {
		return lista;
	}

	public void setLista(List<Modalidade> lista) {
		this.lista = lista;
	}

	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public Campus getCampusSelecionado() {
		return campusSelecionado;
	}

	public void setCampusSelecionado(Campus campusSelecionado) {
		this.campusSelecionado = campusSelecionado;
	}

	public List<Modalidade> getListaAdm() {
		return listaAdm;
	}

	public void setListaAdm(List<Modalidade> listaAdm) {
		this.listaAdm = listaAdm;
	}

}
