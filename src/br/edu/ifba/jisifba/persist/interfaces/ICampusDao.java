package br.edu.ifba.jisifba.persist.interfaces;

import java.util.List;

import br.edu.ifba.jisifba.persist.entities.Campus;

public interface ICampusDao {
	
	public List<Campus> findAll();
	
	public Campus insert(Campus campus);
	
	public Campus update(Campus campus);
	
	public void delete(Campus campus);
	
	public Campus findById(Campus campus);
	
}
