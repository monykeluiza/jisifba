package br.edu.ifba.jisifba.persist.interfaces;

import java.util.List;

import br.edu.ifba.jisifba.persist.entities.Modalidade;
import br.edu.ifba.jisifba.persist.entities.Servidor;

public interface IModalidadeDao {
	
	public List<Modalidade> findAll();
	
	public Modalidade insert(Modalidade modalidade);
	
	public Modalidade update(Modalidade modalidade);
	
	public void delete(Modalidade modalidade);
	
	public Modalidade findById(Modalidade modalidade);
	
	public List<Modalidade> findAtivas(Servidor servidor);
	
}
