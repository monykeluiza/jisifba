package br.edu.ifba.jisifba.persist.interfaces;

import java.util.List;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Servidor;

public interface IServidorDao {
	
	public List<Servidor> findAll();
	
	public Servidor insert(Servidor servidor);
	
	public Servidor update(Servidor servidor);
	
	public void delete(Servidor servidor);
	
	public Servidor findById(Servidor servidor);
	
	public Servidor findBySiape(Servidor servidor);

	public Servidor findBySiapeCpf(Servidor servidor);

	public List<Servidor> findByCampusSolicitante(Campus campus);
	
	public List<Servidor> findByCampusHomologados(Campus campus);
	
	public List<Servidor> findSolicitantes();
	
	public List<Servidor> findHomologados();
	
}
