package br.edu.ifba.jisifba.persist.interfaces;

import java.util.List;

import br.edu.ifba.jisifba.persist.entities.Equipe;

public interface IEquipeDao {
	
	public List<Equipe> findAll();
	
	public Equipe insert(Equipe equipe);
	
	public Equipe update(Equipe equipe);
	
	public void delete(Equipe equipe);
	
	public Equipe findById(Equipe equipe);
	
	public Equipe findByNome(Equipe equipe);
	
}
