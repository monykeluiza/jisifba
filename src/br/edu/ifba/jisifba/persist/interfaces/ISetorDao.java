package br.edu.ifba.jisifba.persist.interfaces;

import java.util.List;

import br.edu.ifba.jisifba.persist.entities.Setor;

public interface ISetorDao {
	
	public List<Setor> findAll();
	
	public Setor insert(Setor setor);
	
	public Setor update(Setor setor);
	
	public void delete(Setor setor);
	
	public Setor findById(Setor setor);
	
}
