package br.edu.ifba.jisifba.persist.interfaces;

import br.edu.ifba.jisifba.persist.entities.Setor;
import br.edu.ifba.jisifba.persist.entities.Status;

public interface IStatusDao {
	
	public void inserir(Status status);
	
	public Setor findById(Setor setor);

}
