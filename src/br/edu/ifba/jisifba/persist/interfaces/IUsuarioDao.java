package br.edu.ifba.jisifba.persist.interfaces;

import java.util.List;

import br.edu.ifba.jisifba.persist.entities.Usuario;

public interface IUsuarioDao {
	
	public Usuario salvar(Usuario usuario);
	
	public List<Usuario> pesquisarTodos();
	
	public List<Usuario> pesquisarPorNome(Usuario usuario);
	
	public Usuario atualizar(Usuario usuario);
	
	public Usuario pesquisarPorId(Usuario usuario);
	
	public Usuario pesquisarPorLoginAtivo(Usuario usuario);
	
	public Usuario pesquisarPorLoginSenhaAtivo(Usuario usuario);
	
	public Usuario pesquisarPorLogin(Usuario usuario);

}
