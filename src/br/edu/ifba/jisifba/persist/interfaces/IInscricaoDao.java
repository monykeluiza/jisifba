package br.edu.ifba.jisifba.persist.interfaces;

import java.util.List;

import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Inscricao;
import br.edu.ifba.jisifba.persist.entities.Modalidade;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.entities.Status;

public interface IInscricaoDao {
	
	public List<Inscricao> findAll();
	
	public Inscricao insert(Inscricao inscricao) throws Exception;
	
	public Inscricao update(Inscricao inscricao);
	
	public void delete(Inscricao inscricao);
	
	public Inscricao findById(Inscricao inscricao);

	public List<Inscricao> findByServidor(Servidor servidor);

	public List<Inscricao> findByServidorAtivas(Servidor servidor);
	
	public List<Inscricao> findByCampus(Campus campus);
	
	public List<Inscricao> findByCampusModalidade(Campus campus, Modalidade modalidade);
	
	public List<Inscricao> findByModalidade(Modalidade modalidade);
	
	public List<Inscricao> findByCampusModalidadeStatus(Campus campus, Modalidade modalidade, Status status);
	
	public List<Inscricao> findByCampusStatus(Campus campus, Status status);
	
	public List<Inscricao> findByStatus(Status status);
	
	public List<Inscricao> findByTimeAvulso();
	
	public List<Inscricao> findByFilter(Inscricao inscricao);
	
	public List<Inscricao> findByCampusStatuSolicitacoes(Campus campus);
	
	public List<Inscricao> findByCampusModalidadeStatuSolicitacoes(Campus campus, Modalidade modalidade);
	
}
