/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifba.jisifba.persist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Monique
 */
@Embeddable
public class CampusModalidadePK implements Serializable {

	private static final long serialVersionUID = -8231322770698346108L;
	@Basic(optional = false)
    @Column(name = "campus_id")
    private int campusId;
    @Basic(optional = false)
    @Column(name = "modalidade_id")
    private int modalidadeId;

    public CampusModalidadePK() {
    }

    public CampusModalidadePK(int campusId, int modalidadeId) {
        this.campusId = campusId;
        this.modalidadeId = modalidadeId;
    }

    public int getCampusId() {
        return campusId;
    }

    public void setCampusId(int campusId) {
        this.campusId = campusId;
    }

    public int getModalidadeId() {
        return modalidadeId;
    }

    public void setModalidadeId(int modalidadeId) {
        this.modalidadeId = modalidadeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) campusId;
        hash += (int) modalidadeId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CampusModalidadePK)) {
            return false;
        }
        CampusModalidadePK other = (CampusModalidadePK) object;
        if (this.campusId != other.campusId) {
            return false;
        }
        if (this.modalidadeId != other.modalidadeId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CampusModalidadePK[ campusId=" + campusId + ", modalidadeId=" + modalidadeId + " ]";
    }
    
}
