/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifba.jisifba.persist.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Monique
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Modalidade.findAll", query = "SELECT m FROM Modalidade m")
    , @NamedQuery(name = "Modalidade.findById", query = "SELECT m FROM Modalidade m WHERE m.id = :id")
    , @NamedQuery(name = "Modalidade.findByNome", query = "SELECT m FROM Modalidade m WHERE m.nome = :nome")
    , @NamedQuery(name = "Modalidade.findByAtiva", query = "SELECT m FROM Modalidade m WHERE m.ativa = :ativa")
    , @NamedQuery(name = "Modalidade.findByQtdTitular", query = "SELECT m FROM Modalidade m WHERE m.qtdTitular = :qtdTitular")
    , @NamedQuery(name = "Modalidade.findByQtdReserva", query = "SELECT m FROM Modalidade m WHERE m.qtdReserva = :qtdReserva")
    , @NamedQuery(name = "Modalidade.findAtivas", query = "SELECT m FROM Modalidade m WHERE m.ativa = true")
    , @NamedQuery(name = "Modalidade.findAtivasSexo", query = "SELECT m FROM Modalidade m WHERE m.ativa = true and (m.sexo = :sexo or m.sexo = 'MF' or m.sexo = 'N/A')")
    , @NamedQuery(name = "Modalidade.findBySexo", query = "SELECT m FROM Modalidade m WHERE m.sexo = :sexo")})
public class Modalidade implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    private String nome;
    @Basic(optional = false)
    private Boolean ativa;
    @Basic(optional = false)
    @Column(name = "qtd_titular")
    private int qtdTitular;
    @Basic(optional = false)
    @Column(name = "qtd_reserva")
    private int qtdReserva;
    @Basic(optional = false)
    private String sexo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modalidade")
    private List<Inscricao> inscricaoList;
    @OneToMany(mappedBy = "modalidadeId")
    private List<Calendario> calendarioList;
    @JoinColumn(name = "tipo_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Tipo tipoId;
    @Basic(optional = false)
    @Column(name = "vaga_por_campus")
    private int vagaPorCampus;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modalidade")
    private List<CampusModalidade> campusModalidadeList;
    @Transient
    private String sexoDesc;
    @Transient
    private int totalInscricoes;
    @Transient
    private int totalInscricoesDoCampus;
    @Transient
    private int totalInscricoesHomologadasDoCampus;
    @Transient
    private int totalInscricoesHomologadas;
    @Transient
    private int totalServidoresSolicitantes;
    @Transient
    private int totalServidoresHomologados;

    public Modalidade() {
    }

    public Modalidade(Integer id) {
        this.id = id;
    }

    public Modalidade(Integer id, String nome, Boolean ativa, int qtdTitular, int qtdReserva, String sexo) {
        this.id = id;
        this.nome = nome;
        this.ativa = ativa;
        this.qtdTitular = qtdTitular;
        this.qtdReserva = qtdReserva;
        this.sexo = sexo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getAtiva() {
        return ativa;
    }

    public void setAtiva(Boolean ativa) {
        this.ativa = ativa;
    }

    public int getQtdTitular() {
        return qtdTitular;
    }

    public void setQtdTitular(int qtdTitular) {
        this.qtdTitular = qtdTitular;
    }

    public int getQtdReserva() {
        return qtdReserva;
    }

    public void setQtdReserva(int qtdReserva) {
        this.qtdReserva = qtdReserva;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @XmlTransient
    public List<Inscricao> getInscricaoList() {
        return inscricaoList;
    }

    public void setInscricaoList(List<Inscricao> inscricaoList) {
        this.inscricaoList = inscricaoList;
    }

    @XmlTransient
    public List<Calendario> getCalendarioList() {
        return calendarioList;
    }

    public void setCalendarioList(List<Calendario> calendarioList) {
        this.calendarioList = calendarioList;
    }

    public Tipo getTipoId() {
    	if (tipoId == null) {
    		tipoId = new Tipo();
    	}
        return tipoId;
    }

    public void setTipoId(Tipo tipoId) {
        this.tipoId = tipoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Modalidade)) {
            return false;
        }
        Modalidade other = (Modalidade) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_crm.Modalidade[ id=" + id + " ]";
    }

	public List<CampusModalidade> getCampusModalidadeList() {
		return campusModalidadeList;
	}

	public void setCampusModalidadeList(List<CampusModalidade> campusModalidadeList) {
		this.campusModalidadeList = campusModalidadeList;
	}

	public int getVagaPorCampus() {
		return vagaPorCampus;
	}

	public void setVagaPorCampus(int vagaPorCampus) {
		this.vagaPorCampus = vagaPorCampus;
	}

	public String getSexoDesc() {
		return (sexo.equals("M")?"MASCULINO":(sexo.equals("F")?"FEMININO":(sexo.equals("MF")?"MISTO":"")));
	}

	public void setSexoDesc(String sexoDesc) {
		this.sexoDesc = sexoDesc;
	}

	public int getTotalInscricoes() {
		return totalInscricoes;
	}

	public void setTotalInscricoes(int totalInscricoes) {
		this.totalInscricoes = totalInscricoes;
	}

	public int getTotalInscricoesDoCampus() {
		return totalInscricoesDoCampus;
	}

	public void setTotalInscricoesDoCampus(int totalInscricoesDoCampus) {
		this.totalInscricoesDoCampus = totalInscricoesDoCampus;
	}

	public int getTotalInscricoesHomologadasDoCampus() {
		return totalInscricoesHomologadasDoCampus;
	}

	public void setTotalInscricoesHomologadasDoCampus(int totalInscricoesHomologadasDoCampus) {
		this.totalInscricoesHomologadasDoCampus = totalInscricoesHomologadasDoCampus;
	}

	public int getTotalInscricoesHomologadas() {
		return totalInscricoesHomologadas;
	}

	public void setTotalInscricoesHomologadas(int totalInscricoesHomologadas) {
		this.totalInscricoesHomologadas = totalInscricoesHomologadas;
	}

	public int getTotalServidoresSolicitantes() {
		return totalServidoresSolicitantes;
	}

	public void setTotalServidoresSolicitantes(int totalServidoresSolicitantes) {
		this.totalServidoresSolicitantes = totalServidoresSolicitantes;
	}

	public int getTotalServidoresHomologados() {
		return totalServidoresHomologados;
	}

	public void setTotalServidoresHomologados(int totalServidoresHomologados) {
		this.totalServidoresHomologados = totalServidoresHomologados;
	}
    
}
