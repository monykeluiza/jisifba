/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifba.jisifba.persist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Monique
 */
@Entity
@Table(name = "campus_modalidade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CampusModalidade.findAll", query = "SELECT c FROM CampusModalidade c")
    , @NamedQuery(name = "CampusModalidade.findByCampusId", query = "SELECT c FROM CampusModalidade c WHERE c.campusModalidadePK.campusId = :campusId")
    , @NamedQuery(name = "CampusModalidade.findByModalidadeId", query = "SELECT c FROM CampusModalidade c WHERE c.campusModalidadePK.modalidadeId = :modalidadeId")
    , @NamedQuery(name = "CampusModalidade.findByVagas", query = "SELECT c FROM CampusModalidade c WHERE c.vagas = :vagas")})
public class CampusModalidade implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CampusModalidadePK campusModalidadePK;
    @Basic(optional = false)
    private int vagas;
    @JoinColumn(name = "campus_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Campus campus;
    @JoinColumn(name = "modalidade_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Modalidade modalidade;

    public CampusModalidade() {
    }

    public CampusModalidade(CampusModalidadePK campusModalidadePK) {
        this.campusModalidadePK = campusModalidadePK;
    }

    public CampusModalidade(CampusModalidadePK campusModalidadePK, int vagas) {
        this.campusModalidadePK = campusModalidadePK;
        this.vagas = vagas;
    }

    public CampusModalidade(int campusId, int modalidadeId) {
        this.campusModalidadePK = new CampusModalidadePK(campusId, modalidadeId);
    }

    public CampusModalidadePK getCampusModalidadePK() {
        return campusModalidadePK;
    }

    public void setCampusModalidadePK(CampusModalidadePK campusModalidadePK) {
        this.campusModalidadePK = campusModalidadePK;
    }

    public int getVagas() {
        return vagas;
    }

    public void setVagas(int vagas) {
        this.vagas = vagas;
    }

    public Campus getCampus() {
        return campus;
    }

    public void setCampus(Campus campus) {
        this.campus = campus;
    }

    public Modalidade getModalidade() {
        return modalidade;
    }

    public void setModalidade(Modalidade modalidade) {
        this.modalidade = modalidade;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (campusModalidadePK != null ? campusModalidadePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CampusModalidade)) {
            return false;
        }
        CampusModalidade other = (CampusModalidade) object;
        if ((this.campusModalidadePK == null && other.campusModalidadePK != null) || (this.campusModalidadePK != null && !this.campusModalidadePK.equals(other.campusModalidadePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_crm.CampusModalidade[ campusModalidadePK=" + campusModalidadePK + " ]";
    }
    
}
