/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifba.jisifba.persist.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Monique
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servidor.findAll", query = "SELECT s FROM Servidor s")
    , @NamedQuery(name = "Servidor.findById", query = "SELECT s FROM Servidor s WHERE s.id = :id")
    , @NamedQuery(name = "Servidor.findByNome", query = "SELECT s FROM Servidor s WHERE s.nome = :nome")
    , @NamedQuery(name = "Servidor.findByDataNascimento", query = "SELECT s FROM Servidor s WHERE s.dataNascimento = :dataNascimento")
    , @NamedQuery(name = "Servidor.findByEmailInstitucional", query = "SELECT s FROM Servidor s WHERE s.emailInstitucional = :emailInstitucional")
    , @NamedQuery(name = "Servidor.findBySiape", query = "SELECT s FROM Servidor s WHERE s.siape = :siape")
    , @NamedQuery(name = "Servidor.findByEmailPessoal", query = "SELECT s FROM Servidor s WHERE s.emailPessoal = :emailPessoal")
    , @NamedQuery(name = "Servidor.findBySexo", query = "SELECT s FROM Servidor s WHERE s.sexo = :sexo")
    , @NamedQuery(name = "Servidor.findByCpf", query = "SELECT s FROM Servidor s WHERE s.cpf = :cpf")
    , @NamedQuery(name = "Servidor.findByIsDiretorCampus", query = "SELECT s FROM Servidor s WHERE s.isDiretorCampus = :isDiretorCampus")
    , @NamedQuery(name = "Servidor.findByIsChefeDelegacao", query = "SELECT s FROM Servidor s WHERE s.isChefeDelegacao = :isChefeDelegacao")
    , @NamedQuery(name = "Servidor.findByHospedagem", query = "SELECT s FROM Servidor s WHERE s.hospedagem = :hospedagem")
    , @NamedQuery(name = "Servidor.findByQtdAcompanhantesAdultos", query = "SELECT s FROM Servidor s WHERE s.qtdAcompanhantesAdultos = :qtdAcompanhantesAdultos")
    , @NamedQuery(name = "Servidor.findByQtdAcompanhantesCriancas", query = "SELECT s FROM Servidor s WHERE s.qtdAcompanhantesCriancas = :qtdAcompanhantesCriancas")
    , @NamedQuery(name = "Servidor.findByPlanoDeSaude", query = "SELECT s FROM Servidor s WHERE s.planoDeSaude = :planoDeSaude")
    , @NamedQuery(name = "Servidor.findByAlergias", query = "SELECT s FROM Servidor s WHERE s.alergias = :alergias")
    , @NamedQuery(name = "Servidor.findByDoencas", query = "SELECT s FROM Servidor s WHERE s.doencas = :doencas")
    , @NamedQuery(name = "Servidor.findBySiapeCpf", query = "SELECT s FROM Servidor s WHERE s.siape = :siape and s.cpf = :cpf")
    , @NamedQuery(name = "Servidor.findByNomeContatoEmergencia", query = "SELECT s FROM Servidor s WHERE s.nomeContatoEmergencia = :nomeContatoEmergencia")
    , @NamedQuery(name = "Servidor.findSolicitantesByCampus", query = "SELECT s FROM Servidor s WHERE s.setorId.campusId.id = :campusId and s.id in (select i.inscricaoPK.servidorId from Inscricao i where i.statusId.id not in (1,7))")
    , @NamedQuery(name = "Servidor.findInscritosByCampus", query = "SELECT s FROM Servidor s WHERE s.setorId.campusId.id = :campusId and s.id in (select i.inscricaoPK.servidorId from Inscricao i where i.statusId.id in (5))")
    , @NamedQuery(name = "Servidor.findSolicitantes", query = "SELECT s FROM Servidor s WHERE s.id in (select i.inscricaoPK.servidorId from Inscricao i where i.statusId.id not in (1,7))")
    , @NamedQuery(name = "Servidor.findHomologados", query = "SELECT s FROM Servidor s WHERE s.id in (select i.inscricaoPK.servidorId from Inscricao i where i.statusId.id in (5))")
    , @NamedQuery(name = "Servidor.findByTelefoneContatoEmergencia", query = "SELECT s FROM Servidor s WHERE s.telefoneContatoEmergencia = :telefoneContatoEmergencia")})
public class Servidor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    private String nome;
    @Basic(optional = false)
    @Column(name = "data_nascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;
    @Column(name = "email_institucional")
    private String emailInstitucional;
    private String siape;
    @Column(name = "email_pessoal")
    private String emailPessoal;
    @Basic(optional = false)
    private Character sexo;
    @Basic(optional = false)
    private String cpf;
    @Basic(optional = false)
    @Column(name = "is_diretor_campus")
    private Boolean isDiretorCampus;
    @Basic(optional = false)
    @Column(name = "is_chefe_delegacao")
    private Boolean isChefeDelegacao;
    @Basic(optional = false)
    private Boolean hospedagem;
    @Column(name = "qtd_acompanhantes_adultos")
    private Integer qtdAcompanhantesAdultos;
    @Column(name = "qtd_acompanhantes_criancas")
    private Integer qtdAcompanhantesCriancas;
    @Column(name = "plano_de_saude")
    private String planoDeSaude;
    private String alergias;
    private String doencas;
    @Column(name = "nome_contato_emergencia")
    private String nomeContatoEmergencia;
    @Column(name = "telefone_contato_emergencia")
    private String telefoneContatoEmergencia;
    @JoinColumn(name = "setor_id", referencedColumnName = "id")
    @ManyToOne
    private Setor setorId;
    @JoinColumn(name = "usuario_id", referencedColumnName = "id")
    @ManyToOne
    private Usuario userId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "servidor")
    private List<Inscricao> inscricaoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "servidorId",fetch=FetchType.EAGER)
    private List<Docs> docsList;

    public Servidor() {
    }

    public Servidor(Integer id) {
        this.id = id;
    }

    public Servidor(Integer id, String nome, Date dataNascimento, Character sexo, String cpf, Boolean isDiretorCampus, Boolean isChefeDelegacao, Boolean hospedagem, String planoDeSaude, String nomeContatoEmergencia, String telefoneContatoEmergencia) {
        this.id = id;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.cpf = cpf;
        this.isDiretorCampus = isDiretorCampus;
        this.isChefeDelegacao = isChefeDelegacao;
        this.hospedagem = hospedagem;
        this.planoDeSaude = planoDeSaude;
        this.nomeContatoEmergencia = nomeContatoEmergencia;
        this.telefoneContatoEmergencia = telefoneContatoEmergencia;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getEmailInstitucional() {
        return emailInstitucional;
    }

    public void setEmailInstitucional(String emailInstitucional) {
        this.emailInstitucional = emailInstitucional;
    }

    public String getSiape() {
        return siape;
    }

    public void setSiape(String siape) {
        this.siape = siape;
    }

    public String getEmailPessoal() {
        return emailPessoal;
    }

    public void setEmailPessoal(String emailPessoal) {
        this.emailPessoal = emailPessoal;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Boolean getIsDiretorCampus() {
    	if (isDiretorCampus == null) 
    		isDiretorCampus = true;
        return isDiretorCampus;
    }

    public void setIsDiretorCampus(Boolean isDiretorCampus) {
        this.isDiretorCampus = isDiretorCampus;
    }

    public Boolean getIsChefeDelegacao() {
    	if (isChefeDelegacao == null) 
    			isChefeDelegacao = true;
        return isChefeDelegacao;
    }

    public void setIsChefeDelegacao(Boolean isChefeDelegacao) {
        this.isChefeDelegacao = isChefeDelegacao;
    }

    public Boolean getHospedagem() {
//    	if (hospedagem == null) 
//    		hospedagem = true;
        return hospedagem;
    }

    public void setHospedagem(Boolean hospedagem) {
        this.hospedagem = hospedagem;
    }

    public Integer getQtdAcompanhantesAdultos() {
        return qtdAcompanhantesAdultos;
    }

    public void setQtdAcompanhantesAdultos(Integer qtdAcompanhantesAdultos) {
        this.qtdAcompanhantesAdultos = qtdAcompanhantesAdultos;
    }

    public Integer getQtdAcompanhantesCriancas() {
        return qtdAcompanhantesCriancas;
    }

    public void setQtdAcompanhantesCriancas(Integer qtdAcompanhantesCriancas) {
        this.qtdAcompanhantesCriancas = qtdAcompanhantesCriancas;
    }

    public String getPlanoDeSaude() {
        return planoDeSaude;
    }

    public void setPlanoDeSaude(String planoDeSaude) {
        this.planoDeSaude = planoDeSaude;
    }

    public String getAlergias() {
        return alergias;
    }

    public void setAlergias(String alergias) {
        this.alergias = alergias;
    }

    public String getDoencas() {
        return doencas;
    }

    public void setDoencas(String doencas) {
        this.doencas = doencas;
    }

    public String getNomeContatoEmergencia() {
        return nomeContatoEmergencia;
    }

    public void setNomeContatoEmergencia(String nomeContatoEmergencia) {
        this.nomeContatoEmergencia = nomeContatoEmergencia;
    }

    public String getTelefoneContatoEmergencia() {
        return telefoneContatoEmergencia;
    }

    public void setTelefoneContatoEmergencia(String telefoneContatoEmergencia) {
        this.telefoneContatoEmergencia = telefoneContatoEmergencia;
    }

    public Setor getSetorId() {
    	if (setorId == null) 
    		setorId = new Setor();
        return setorId;
    }

    public void setSetorId(Setor setorId) {
        this.setorId = setorId;
    }

    public Usuario getUserId() {
    	if (userId == null)
    		userId = new Usuario();
        return userId;
    }

    public void setUserId(Usuario userId) {
        this.userId = userId;
    }

    @XmlTransient
    public List<Inscricao> getInscricaoList() {
        return inscricaoList;
    }

    public void setInscricaoList(List<Inscricao> inscricaoList) {
        this.inscricaoList = inscricaoList;
    }

    @XmlTransient
    public List<Docs> getDocsList() {
        return docsList;
    }

    public void setDocsList(List<Docs> docsList) {
        this.docsList = docsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servidor)) {
            return false;
        }
        Servidor other = (Servidor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Servidor[ id=" + id + " ]";
    }
    
}
