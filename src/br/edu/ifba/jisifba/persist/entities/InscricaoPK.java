/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifba.jisifba.persist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Monique
 */
@Embeddable
public class InscricaoPK implements Serializable {

	private static final long serialVersionUID = -4413847254308767426L;
	@Basic(optional = false)
    @Column(name = "servidor_id")
    private int servidorId;
    @Basic(optional = false)
    @Column(name = "modalidade_id")
    private int modalidadeId;

    public InscricaoPK() {
    }

    public InscricaoPK(int servidorId, int modalidadeId) {
        this.servidorId = servidorId;
        this.modalidadeId = modalidadeId;
    }

    public int getServidorId() {
        return servidorId;
    }

    public void setServidorId(int servidorId) {
        this.servidorId = servidorId;
    }

    public int getModalidadeId() {
        return modalidadeId;
    }

    public void setModalidadeId(int modalidadeId) {
        this.modalidadeId = modalidadeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) servidorId;
        hash += (int) modalidadeId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InscricaoPK)) {
            return false;
        }
        InscricaoPK other = (InscricaoPK) object;
        if (this.servidorId != other.servidorId) {
            return false;
        }
        if (this.modalidadeId != other.modalidadeId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_crm.InscricaoPK[ servidorId=" + servidorId + ", modalidadeId=" + modalidadeId + " ]";
    }
    
}
