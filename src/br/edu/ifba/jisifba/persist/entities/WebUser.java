package br.edu.ifba.jisifba.persist.entities;


public class WebUser {    

    private String cn;
    private String displayName;
    
    public WebUser() {}

    public WebUser(String cn, String displayName) {
        this.cn = cn;
        this.displayName = displayName;      
    }
   
    
	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
