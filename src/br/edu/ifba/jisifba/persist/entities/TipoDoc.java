/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifba.jisifba.persist.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Monique
 */
@Entity
@Table(name = "tipo_doc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoDoc.findAll", query = "SELECT t FROM TipoDoc t")
    , @NamedQuery(name = "TipoDoc.findById", query = "SELECT t FROM TipoDoc t WHERE t.id = :id")
    , @NamedQuery(name = "TipoDoc.findByNome", query = "SELECT t FROM TipoDoc t WHERE t.nome = :nome")})
public class TipoDoc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    private String nome;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoDocId")
    private List<Docs> docsList;
    
    public static final int DOCUMENTO = 1;
    public static final int ATESTADO = 2;
    public static final int FOTO = 3;
    public static final int TERMO = 4;

    public TipoDoc() {
    }

    public TipoDoc(Integer id) {
        this.id = id;
    }

    public TipoDoc(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @XmlTransient
    public List<Docs> getDocsList() {
        return docsList;
    }

    public void setDocsList(List<Docs> docsList) {
        this.docsList = docsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDoc)) {
            return false;
        }
        TipoDoc other = (TipoDoc) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_crm.TipoDoc[ id=" + id + " ]";
    }
    
}
