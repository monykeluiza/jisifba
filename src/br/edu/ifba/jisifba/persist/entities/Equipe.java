/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifba.jisifba.persist.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Monique
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Equipe.findAll", query = "SELECT e FROM Equipe e")
    , @NamedQuery(name = "Equipe.findById", query = "SELECT e FROM Equipe e WHERE e.id = :id")
    , @NamedQuery(name = "Equipe.findByNome", query = "SELECT e FROM Equipe e WHERE e.nome = :nome")
    , @NamedQuery(name = "Equipe.findByNomeModalidade", query = "SELECT e FROM Equipe e WHERE e.nome = :nome and e.modalidade =  :modalidade")
    , @NamedQuery(name = "Equipe.findByModalidade", query = "SELECT e FROM Equipe e WHERE e.modalidade = :modalidade")})
public class Equipe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    private String nome;
    @Basic(optional = false)
    private String modalidade;
    @OneToMany(mappedBy = "equipeId", fetch = FetchType.EAGER)
    private List<Inscricao> inscricaoList;
    @Transient
    private int qtdTitularCadastrado;
    @Transient
    private int qtdReservaCadastrado;
    @Transient
    private int qtdTitularHomologado;
    @Transient
    private int qtdReservaHomologado;

    public Equipe() {
    }

    public Equipe(Integer id) {
        this.id = id;
    }

    public Equipe(Integer id, String nome, String modalidade) {
        this.id = id;
        this.nome = nome;
        this.modalidade = modalidade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getModalidade() {
        return modalidade;
    }

    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }

    @XmlTransient
    public List<Inscricao> getInscricaoList() {
        return inscricaoList;
    }

    public void setInscricaoList(List<Inscricao> inscricaoList) {
        this.inscricaoList = inscricaoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Equipe)) {
            return false;
        }
        Equipe other = (Equipe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Equipe[ id=" + id + " ]";
    }

	public int getQtdTitularCadastrado() {
		qtdTitularCadastrado = 0;
		for (Inscricao inscricao : inscricaoList) {
			if (inscricao.getTitular() && !inscricao.getStatusId().getId().equals(Status.CANCELADO)){
				qtdTitularCadastrado++;
			}
		}
		return qtdTitularCadastrado;
	}

	public void setQtdTitularCadastrado(int qtdTitularCadastrado) {
		this.qtdTitularCadastrado = qtdTitularCadastrado;
	}

	public int getQtdReservaCadastrado() {
		qtdReservaCadastrado = 0;
		for (Inscricao inscricao : inscricaoList) {
			if (!inscricao.getTitular() && !inscricao.getStatusId().getId().equals(Status.CANCELADO)){
				qtdReservaCadastrado++;
			}
		}
		return qtdReservaCadastrado;
	}

	public void setQtdReservaCadastrado(int qtdReservaCadastrado) {
		this.qtdReservaCadastrado = qtdReservaCadastrado;
	}

	public int getQtdTitularHomologado() {
		qtdTitularHomologado = 0;
		for (Inscricao inscricao : inscricaoList) {
			if (inscricao.getTitular() && inscricao.getStatusId().getId().equals(Status.HOMOLOGADO)){
				qtdTitularHomologado++;
			}
		}
		return qtdTitularHomologado;
	}

	public void setQtdTitularHomologado(int qtdTitularHomologado) {
		this.qtdTitularHomologado = qtdTitularHomologado;
	}

	public int getQtdReservaHomologado() {
		qtdReservaHomologado = 0;
		for (Inscricao inscricao : inscricaoList) {
			if (!inscricao.getTitular() && inscricao.getStatusId().getId().equals(Status.HOMOLOGADO)){
				qtdReservaHomologado++;
			}
		}
		return qtdReservaHomologado;
	}

	public void setQtdReservaHomologado(int qtdReservaHomologado) {
		this.qtdReservaHomologado = qtdReservaHomologado;
	}
    
}
