/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifba.jisifba.persist.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Monique
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Campus.findAll", query = "SELECT c FROM Campus c")
    , @NamedQuery(name = "Campus.findById", query = "SELECT c FROM Campus c WHERE c.id = :id")
    , @NamedQuery(name = "Campus.findByNome", query = "SELECT c FROM Campus c WHERE c.nome = :nome")
    , @NamedQuery(name = "Campus.findBySigla", query = "SELECT c FROM Campus c WHERE c.sigla = :sigla")
    , @NamedQuery(name = "Campus.findByVagas", query = "SELECT c FROM Campus c WHERE c.vagas = :vagas")})
public class Campus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    private String nome;
    @Basic(optional = false)
    private String sigla;
    @Basic(optional = false)
    private int vagas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "campusId")
    private List<Setor> setorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "campus")
    private List<CampusModalidade> campusModalidadeList;
    @Transient
    private int solicitacoes;
    @Transient
    private int homologacoes;
    @Transient
    private int totalServidoresSolicitantes;
    @Transient
    private int totalServidoresHomologados;

    public Campus() {
    }

    public Campus(Integer id) {
        this.id = id;
    }

    public Campus(Integer id, String nome, String sigla, int vagas) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
        this.vagas = vagas;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public int getVagas() {
        return vagas;
    }

    public void setVagas(int vagas) {
        this.vagas = vagas;
    }

    @XmlTransient
    public List<Setor> getSetorList() {
        return setorList;
    }

    public void setSetorList(List<Setor> setorList) {
        this.setorList = setorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Campus)) {
            return false;
        }
        Campus other = (Campus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_crm.Campus[ id=" + id + " ]";
    }

	public List<CampusModalidade> getCampusModalidadeList() {
		return campusModalidadeList;
	}

	public void setCampusModalidadeList(List<CampusModalidade> campusModalidadeList) {
		this.campusModalidadeList = campusModalidadeList;
	}

	public int getSolicitacoes() {
		return solicitacoes;
	}

	public void setSolicitacoes(int solicitacoes) {
		this.solicitacoes = solicitacoes;
	}

	public int getHomologacoes() {
		return homologacoes;
	}

	public void setHomologacoes(int homologacoes) {
		this.homologacoes = homologacoes;
	}

	public int getTotalServidoresSolicitantes() {
		return totalServidoresSolicitantes;
	}

	public void setTotalServidoresSolicitantes(int totalServidoresSolicitantes) {
		this.totalServidoresSolicitantes = totalServidoresSolicitantes;
	}

	public int getTotalServidoresHomologados() {
		return totalServidoresHomologados;
	}

	public void setTotalServidoresHomologados(int totalServidoresHomologados) {
		this.totalServidoresHomologados = totalServidoresHomologados;
	}
    
}
