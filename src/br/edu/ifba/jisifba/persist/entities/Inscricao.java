/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifba.jisifba.persist.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Monique
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inscricao.findAll", query = "SELECT i FROM Inscricao i")
    , @NamedQuery(name = "Inscricao.findByServidorId", query = "SELECT i FROM Inscricao i WHERE i.inscricaoPK.servidorId = :servidorId")
    , @NamedQuery(name = "Inscricao.findByModalidadeId", query = "SELECT i FROM Inscricao i WHERE i.inscricaoPK.modalidadeId = :modalidadeId")
    , @NamedQuery(name = "Inscricao.findByData", query = "SELECT i FROM Inscricao i WHERE i.data = :data")
    , @NamedQuery(name = "Inscricao.findByServidor", query = "SELECT i FROM Inscricao i WHERE i.servidor.id = :idServidor")
    , @NamedQuery(name = "Inscricao.findByServidorAtivas", query = "SELECT i FROM Inscricao i WHERE i.servidor.id = :idServidor and i.statusId.id not in (4,6,7)")
    , @NamedQuery(name = "Inscricao.findByCampus", query = "SELECT i FROM Inscricao i WHERE i.servidor.setorId.campusId.id = :idCampus")
    , @NamedQuery(name = "Inscricao.findByStatus", query = "SELECT i FROM Inscricao i WHERE i.statusId.id = :statusId")
    , @NamedQuery(name = "Inscricao.findByCampusStatus", query = "SELECT i FROM Inscricao i WHERE i.servidor.setorId.campusId.id = :idCampus and i.statusId.id = :statusId")
    , @NamedQuery(name = "Inscricao.findByCampusModalidade", query = "SELECT i FROM Inscricao i WHERE i.servidor.setorId.campusId.id = :idCampus and  i.inscricaoPK.modalidadeId = :modalidadeId")
    , @NamedQuery(name = "Inscricao.findByCampusModalidadeStatus", query = "SELECT i FROM Inscricao i WHERE i.servidor.setorId.campusId.id = :idCampus and  i.inscricaoPK.modalidadeId = :modalidadeId and i.statusId.id = :statusId")
    , @NamedQuery(name = "Inscricao.findByTimeAvulso", query = "SELECT i FROM Inscricao i WHERE i.timeAvulso = :timeAvulso")
    , @NamedQuery(name = "Inscricao.findByTimeAvulsoConfirmadas", query = "SELECT i FROM Inscricao i WHERE i.timeAvulso = true and i.statusId.id = 5 order by i.modalidade.nome asc")
    , @NamedQuery(name = "Inscricao.findByFilter", query = "SELECT i FROM Inscricao i WHERE "
    														+ "(:servidorId is null or i.inscricaoPK.servidorId = :servidorId) "
    														+ "and (:modalidadeId is null or i.inscricaoPK.modalidadeId = :modalidadeId) "
    														+ "and (:data is null or i.data = :data) "
    														+ "and (:campusId is null or i.servidor.setorId.campusId.id = :campusId) "
    														+ "and (:statusId is null or i.statusId.id = :statusId) "
    														+ "and (:timeAvulso is null or i.timeAvulso = :timeAvulso) "
    														+ "and (:equipeId is null or i.equipeId.id = :equipeId) "
    														+ "and (:nomeServidor is null or :nomeServidor = '' or i.servidor.nome like :nomeServidor)"
    														+ "and (:siape is null or :siape = '' or i.servidor.siape = :siape)"
    														+ "and (:setorId is null or i.servidor.setorId.id = :setorId) "
    														+ "and (:dataInicio is null or :dataFim is null or i.data between :dataInicio and :dataFim) "
    														+ "and (:hospedagem is null or i.servidor.hospedagem = :hospedagem) "
    														)
    , @NamedQuery(name = "Inscricao.findByCampusSolicitados", query = "SELECT i FROM Inscricao i WHERE i.servidor.setorId.campusId.id = :idCampus and i.statusId.id not in (1,7)")
    , @NamedQuery(name = "Inscricao.findByCampusModalidadeSolicitacoes", query = "SELECT i FROM Inscricao i WHERE i.servidor.setorId.campusId.id = :idCampus and i.statusId.id not in (1,7) and i.modalidade.id = :idModalidade")

})
public class Inscricao implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InscricaoPK inscricaoPK;
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @Lob
    private byte[] atestado;
    @Column(name = "time_avulso")
    private Boolean timeAvulso;
    private Boolean titular;
    @JoinColumn(name = "equipe_id", referencedColumnName = "id")
    @ManyToOne
    private Equipe equipeId;
    @JoinColumn(name = "modalidade_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Modalidade modalidade;
    @JoinColumn(name = "servidor_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Servidor servidor;
    @JoinColumn(name = "status_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Status statusId;
    @Transient
    private Date dataInicio;
    @Transient
    private Date dataFim;

    public Inscricao() {
    }

    public Inscricao(InscricaoPK inscricaoPK) {
        this.inscricaoPK = inscricaoPK;
    }

    public Inscricao(InscricaoPK inscricaoPK, Date data) {
        this.inscricaoPK = inscricaoPK;
        this.data = data;
    }

    public Inscricao(int servidorId, int modalidadeId) {
        this.inscricaoPK = new InscricaoPK(servidorId, modalidadeId);
    }

    public InscricaoPK getInscricaoPK() {
        return inscricaoPK;
    }

    public void setInscricaoPK(InscricaoPK inscricaoPK) {
        this.inscricaoPK = inscricaoPK;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public byte[] getAtestado() {
        return atestado;
    }

    public void setAtestado(byte[] atestado) {
        this.atestado = atestado;
    }

    public Boolean getTimeAvulso() {
        return timeAvulso;
    }

    public void setTimeAvulso(Boolean timeAvulso) {
    	if (timeAvulso == null) 
    		timeAvulso = true;
        this.timeAvulso = timeAvulso;
    }

    public Boolean getTitular() {
    	if (titular == null) 
    		titular = true;
        return titular;
    }

    public void setTitular(Boolean titular) {
        this.titular = titular;
    }

    public Equipe getEquipeId() {
    	if (equipeId == null)
    		equipeId = new Equipe();
        return equipeId;
    }

    public void setEquipeId(Equipe equipeId) {
        this.equipeId = equipeId;
    }

    public Modalidade getModalidade() {
    	if (modalidade == null)
    		modalidade = new Modalidade();
        return modalidade;
    }

    public void setModalidade(Modalidade modalidade) {
        this.modalidade = modalidade;
    }

    public Servidor getServidor() {
    	if (servidor == null)
    		servidor = new Servidor();
        return servidor;
    }

    public void setServidor(Servidor servidor) {
        this.servidor = servidor;
    }

    public Status getStatusId() {
    	if (statusId == null)
    		statusId = new Status();
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inscricaoPK != null ? inscricaoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inscricao)) {
            return false;
        }
        Inscricao other = (Inscricao) object;
        if ((this.inscricaoPK == null && other.inscricaoPK != null) || (this.inscricaoPK != null && !this.inscricaoPK.equals(other.inscricaoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_crm.Inscricao[ inscricaoPK=" + inscricaoPK + " ]";
    }

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
    
}
