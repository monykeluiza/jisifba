/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifba.jisifba.persist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Monique
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Docs.findAll", query = "SELECT d FROM Docs d")
    , @NamedQuery(name = "Docs.findById", query = "SELECT d FROM Docs d WHERE d.id = :id")
    , @NamedQuery(name = "Docs.findByData", query = "SELECT d FROM Docs d WHERE d.data = :data")
    , @NamedQuery(name = "Docs.findByNomeArquivo", query = "SELECT d FROM Docs d WHERE d.nomeArquivo = :nomeArquivo")})
public class Docs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @Lob
    private byte[] documento;
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @Basic(optional = false)
    @Column(name = "nome_arquivo")
    private String nomeArquivo;
    @JoinColumn(name = "servidor_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Servidor servidorId;
    @JoinColumn(name = "tipo_doc_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoDoc tipoDocId;

    public Docs() {
    }

    public Docs(Integer id) {
        this.id = id;
    }

    public Docs(Integer id, byte[] documento, Date data, String nomeArquivo) {
        this.id = id;
        this.documento = documento;
        this.data = data;
        this.nomeArquivo = nomeArquivo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getDocumento() {
        return documento;
    }

    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public Servidor getServidorId() {
        return servidorId;
    }

    public void setServidorId(Servidor servidorId) {
        this.servidorId = servidorId;
    }

    public TipoDoc getTipoDocId() {
        return tipoDocId;
    }

    public void setTipoDocId(TipoDoc tipoDocId) {
        this.tipoDocId = tipoDocId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Docs)) {
            return false;
        }
        Docs other = (Docs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_crm.Docs[ id=" + id + " ]";
    }
    
}
