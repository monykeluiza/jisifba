package br.edu.ifba.jisifba.persist.entities;

public class Cracha {
	
	private String cargo;
	
	private String imgBackground;
	
	private String nome;
	
	private String campus;
	
	private String siape;
	
	private byte[] foto;

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getImgBackground() {
		return imgBackground;
	}

	public void setImgBackground(String imgBackground) {
		this.imgBackground = imgBackground;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCampus() {
		return campus;
	}

	public void setCampus(String campus) {
		this.campus = campus;
	}

	public String getSiape() {
		return siape;
	}

	public void setSiape(String siape) {
		this.siape = siape;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

}
