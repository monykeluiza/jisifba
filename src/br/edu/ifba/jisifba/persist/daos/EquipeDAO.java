package br.edu.ifba.jisifba.persist.daos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.validation.ConstraintViolationException;

import br.edu.ifba.jisifba.persist.DataAccess;
import br.edu.ifba.jisifba.persist.entities.Equipe;
import br.edu.ifba.jisifba.persist.interfaces.IEquipeDao;

@Stateless
public class EquipeDAO extends DataAccess<Equipe> implements IEquipeDao{
	
	@Override
	public List<Equipe> findAll() {
		return super.findWithNamedQuery("Equipe.findAll");
	}

	@Override
	public Equipe insert(Equipe equipe) {
		try {
			Equipe result = super.create(equipe);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return equipe;
		}
	}

	@Override
	public Equipe update(Equipe equipe) {
		try {
			Equipe result = super.update(equipe);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return equipe;
		}
	}

	@Override
	public void delete(Equipe equipe) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Equipe findById(Equipe equipe) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("id", equipe.getId());
		return (Equipe) super.findWithNamedQueryUniqueOrNull("Equipe.findById", parametros);
	}
	
	@Override
	public Equipe findByNome(Equipe equipe) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("nome", equipe.getNome().toUpperCase());
		parametros.put("modalidade", equipe.getModalidade());
		return (Equipe) super.findWithNamedQueryUniqueOrNull("Equipe.findByNomeModalidade", parametros);
	}

}
