package br.edu.ifba.jisifba.persist.daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.validation.ConstraintViolationException;

import br.edu.ifba.jisifba.persist.DataAccess;
import br.edu.ifba.jisifba.persist.entities.Setor;
import br.edu.ifba.jisifba.persist.interfaces.ISetorDao;

@Stateless
public class SetorDAO extends DataAccess<Setor> implements ISetorDao{
	
	@Override
	public List<Setor> findAll() {
		return super.findWithNamedQuery("Setor.findAll");
	}

	@Override
	public Setor insert(Setor setor) {
		try {
			Setor result = super.create(setor);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return setor;
		}
	}

	@Override
	public Setor update(Setor setor) {
		try {
			Setor result = super.update(setor);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return setor;
		}
	}

	@Override
	public void delete(Setor setor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Setor findById(Setor setor) {
		// TODO Auto-generated method stub
		return null;
	}

}
