package br.edu.ifba.jisifba.persist.daos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.validation.ConstraintViolationException;

import br.edu.ifba.jisifba.persist.DataAccess;
import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.interfaces.IServidorDao;

@Stateless
public class ServidorDAO extends DataAccess<Servidor> implements IServidorDao{
	
	@Override
	public List<Servidor> findAll() {
		return super.findWithNamedQuery("Servidor.findAll");
	}

	@Override
	public Servidor insert(Servidor servidor) {
		try {
			Servidor result = super.create(servidor);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return servidor;
		}
	}

	@Override
	public Servidor update(Servidor servidor) {
		try {
			Servidor result = super.update(servidor);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return servidor;
		}
	}

	@Override
	public void delete(Servidor servidor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Servidor findById(Servidor servidor) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("id", servidor.getId());
		return (Servidor) super.findWithNamedQueryUniqueOrNull("Servidor.findById", parametros);
	}

	@Override
	public Servidor findBySiape(Servidor servidor) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("siape", servidor.getSiape());
		return (Servidor) super.findWithNamedQueryUniqueOrNull("Servidor.findBySiape", parametros);
	}
	
	@Override
	public Servidor findBySiapeCpf(Servidor servidor) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("siape", servidor.getSiape());
		parametros.put("cpf", servidor.getCpf());
		return (Servidor) super.findWithNamedQueryUniqueOrNull("Servidor.findBySiapeCpf", parametros);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Servidor> findByCampusSolicitante(Campus campus) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("campusId", campus.getId());
		return super.findWithNamedQuery("Servidor.findSolicitantesByCampus", parametros);
	}
	
	@SuppressWarnings("unchecked")
	public List<Servidor> findByCampusHomologados(Campus campus) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("campusId", campus.getId());
		return super.findWithNamedQuery("Servidor.findInscritosByCampus", parametros);
	}
	
	public List<Servidor> findSolicitantes() {
		return super.findWithNamedQuery("Servidor.findSolicitantes");
	}
	
	public List<Servidor> findHomologados() {
		return super.findWithNamedQuery("Servidor.findHomologados");
	}

}
