package br.edu.ifba.jisifba.persist.daos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.validation.ConstraintViolationException;

import br.edu.ifba.jisifba.persist.DataAccess;
import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.entities.Inscricao;
import br.edu.ifba.jisifba.persist.entities.Modalidade;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.entities.Status;
import br.edu.ifba.jisifba.persist.interfaces.IInscricaoDao;

@Stateless
public class InscricaoDAO extends DataAccess<Inscricao> implements IInscricaoDao{
	
	@Override
	public List<Inscricao> findAll() {
		return super.findWithNamedQuery("Inscricao.findAll");
	}

	@Override
	public Inscricao insert(Inscricao inscricao) throws Exception {
		try {
			Inscricao result = super.create(inscricao);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return inscricao;
		}
	}

	@Override
	public Inscricao update(Inscricao inscricao) {
		try {
			Inscricao result = super.update(inscricao);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return inscricao;
		}
	}

	@Override
	public void delete(Inscricao inscricao) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Inscricao findById(Inscricao inscricao) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Inscricao> findByServidor(Servidor servidor) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("idServidor", servidor.getId());
		return super.findWithNamedQuery("Inscricao.findByServidor", parametros);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Inscricao> findByServidorAtivas(Servidor servidor) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("idServidor", servidor.getId());
		return super.findWithNamedQuery("Inscricao.findByServidorAtivas", parametros);
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> findByCampus(Campus campus) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("idCampus", campus.getId());
		return super.findWithNamedQuery("Inscricao.findByCampus", parametros);
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> findByCampusModalidade(Campus campus, Modalidade modalidade) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("idCampus", campus.getId());
		parametros.put("modalidadeId", modalidade.getId());
		return super.findWithNamedQuery("Inscricao.findByCampusModalidade", parametros);
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> findByModalidade(Modalidade modalidade) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("modalidadeId", modalidade.getId());
		return super.findWithNamedQuery("Inscricao.findByModalidadeId", parametros);
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> findByCampusModalidadeStatus(Campus campus, Modalidade modalidade, Status status) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("statusId", status.getId());
		parametros.put("idCampus", campus.getId());
		parametros.put("modalidadeId", modalidade.getId());
		return super.findWithNamedQuery("Inscricao.findByCampusModalidadeStatus", parametros);
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> findByCampusStatus(Campus campus, Status status) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("statusId", status.getId());
		parametros.put("idCampus", campus.getId());
		return super.findWithNamedQuery("Inscricao.findByCampusStatus", parametros);
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> findByStatus(Status status) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("statusId", status.getId());
		return super.findWithNamedQuery("Inscricao.findByStatus", parametros);
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> findByFilter(Inscricao inscricao) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("servidorId", inscricao.getServidor().getId());
		parametros.put("modalidadeId", inscricao.getModalidade().getId());
		parametros.put("data", inscricao.getData());
		parametros.put("campusId", inscricao.getServidor().getSetorId().getCampusId().getId());
		parametros.put("statusId", inscricao.getStatusId().getId());
		parametros.put("timeAvulso", inscricao.getTimeAvulso());
		parametros.put("equipeId", inscricao.getEquipeId().getId());
		parametros.put("nomeServidor", inscricao.getServidor().getNome() + "%");
		parametros.put("siape", inscricao.getServidor().getSiape());
		parametros.put("setorId", inscricao.getServidor().getSetorId().getId());
		parametros.put("dataInicio", inscricao.getDataInicio());
		parametros.put("dataFim", inscricao.getDataFim());
		parametros.put("hospedagem", inscricao.getServidor().getHospedagem());
		return super.findWithNamedQuery("Inscricao.findByFilter", parametros);
	}
	
	public List<Inscricao> findByTimeAvulso() {
		return super.findWithNamedQuery("Inscricao.findByTimeAvulsoConfirmadas");
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> findByCampusStatuSolicitacoes(Campus campus) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("idCampus", campus.getId());
		return super.findWithNamedQuery("Inscricao.findByCampusSolicitados", parametros);
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> findByCampusModalidadeStatuSolicitacoes(Campus campus, Modalidade modalidade) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("idCampus", campus.getId());
		parametros.put("idModalidade", modalidade.getId());
		return super.findWithNamedQuery("Inscricao.findByCampusModalidadeSolicitacoes", parametros);
	}
	

}
