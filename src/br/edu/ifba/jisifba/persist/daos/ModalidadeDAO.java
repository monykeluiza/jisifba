package br.edu.ifba.jisifba.persist.daos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.validation.ConstraintViolationException;

import br.edu.ifba.jisifba.persist.DataAccess;
import br.edu.ifba.jisifba.persist.entities.Modalidade;
import br.edu.ifba.jisifba.persist.entities.Servidor;
import br.edu.ifba.jisifba.persist.interfaces.IModalidadeDao;

@Stateless
public class ModalidadeDAO extends DataAccess<Modalidade> implements IModalidadeDao{
	
	@Override
	public List<Modalidade> findAll() {
		return super.findWithNamedQuery("Modalidade.findAll");
	}

	@Override
	public Modalidade insert(Modalidade modalidade) {
		try {
			Modalidade result = super.create(modalidade);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return modalidade;
		}
	}

	@Override
	public Modalidade update(Modalidade modalidade) {
		try {
			Modalidade result = super.update(modalidade);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return modalidade;
		}
	}

	@Override
	public void delete(Modalidade modalidade) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Modalidade findById(Modalidade modalidade) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Modalidade> findAtivas(Servidor servidor) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("sexo", servidor.getSexo().toString());
		return super.findWithNamedQuery("Modalidade.findAtivasSexo", parametros);
	}

}
