package br.edu.ifba.jisifba.persist.daos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.validation.ConstraintViolationException;

import br.edu.ifba.jisifba.persist.DataAccess;
import br.edu.ifba.jisifba.persist.entities.Usuario;
import br.edu.ifba.jisifba.persist.interfaces.IUsuarioDao;


@Stateless
public class UsuarioDAO extends DataAccess<Usuario> implements IUsuarioDao {

	@Override
	public Usuario salvar(Usuario usuario) {
		try {
			Usuario usuarioResult = super.create(usuario);
			return usuarioResult;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return usuario;
		}
	}

	@Override
	public List<Usuario> pesquisarTodos() {
		return super.findWithNamedQuery("Usuario.findAll");
	}

	@Override
	public List<Usuario> pesquisarPorNome(Usuario usuario) {
//		Map<String, Object> parametros = new HashMap<String, Object>();
//		parametros.put("nomeGrupo", usuario.getNome().toUpperCase() + "%");
//		return super.findWithNamedQuery("User.findByNome", parametros);
		return null;
	}

	@Override
	public Usuario atualizar(Usuario usuario) {
		Usuario usuarioResult = super.update(usuario);
		return usuarioResult;
	}

	@Override
	public Usuario pesquisarPorId(Usuario usuario) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("id", usuario.getId());
		return (Usuario) super.findWithNamedQueryUniqueOrNull("Usuario.findById", parametros);
	}

	@Override
	public Usuario pesquisarPorLoginAtivo(Usuario usuario) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("username", usuario.getUsername());
		return (Usuario) super.findWithNamedQueryUniqueOrNull("Usuario.findByLoginAtivo", parametros);
	}
	
	@Override
	public Usuario pesquisarPorLoginSenhaAtivo(Usuario usuario) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("username", usuario.getUsername());
		parametros.put("password", usuario.getPassword());
		return (Usuario) super.findWithNamedQueryUniqueOrNull("Usuario.findByLoginSenhaAtivo", parametros);
	}
	
	@Override
	public Usuario pesquisarPorLogin(Usuario usuario) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("username", usuario.getUsername());
		return (Usuario) super.findWithNamedQueryUniqueOrNull("Usuario.findByUsername", parametros);
	}

	
}
