package br.edu.ifba.jisifba.persist.daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.validation.ConstraintViolationException;

import br.edu.ifba.jisifba.persist.DataAccess;
import br.edu.ifba.jisifba.persist.entities.Campus;
import br.edu.ifba.jisifba.persist.interfaces.ICampusDao;

@Stateless
public class CampusDAO extends DataAccess<Campus> implements ICampusDao{
	
	@Override
	public List<Campus> findAll() {
		return super.findWithNamedQuery("Campus.findAll");
	}

	@Override
	public Campus insert(Campus campus) {
		try {
			Campus result = super.create(campus);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return campus;
		}
	}

	@Override
	public Campus update(Campus campus) {
		try {
			Campus result = super.update(campus);
			return result;
		} catch (ConstraintViolationException e) {
			System.out.println(e.getConstraintViolations());
			return campus;
		}
	}

	@Override
	public void delete(Campus campus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Campus findById(Campus campus) {
		// TODO Auto-generated method stub
		return null;
	}

}
