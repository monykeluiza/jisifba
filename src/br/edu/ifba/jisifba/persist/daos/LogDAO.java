package br.edu.ifba.jisifba.persist.daos;

import javax.ejb.Stateless;

import br.edu.ifba.jisifba.persist.DataAccess;
import br.edu.ifba.jisifba.persist.entities.Log;
import br.edu.ifba.jisifba.persist.interfaces.ILogDao;

@Stateless
public class LogDAO extends DataAccess<Log> implements ILogDao {

	@Override
	public void inserir(Log log) {
		super.create(log);
	}
	
	

}
